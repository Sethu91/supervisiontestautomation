//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace UnitTestProject1
{
    using System;
    using System.Collections.Generic;
    
    public partial class Clocking
    {
        public long Id { get; set; }
        public Nullable<int> TenantId { get; set; }
        public System.DateTime ClockingDateTime { get; set; }
        public System.DateTime AdjustedClockingDateTime { get; set; }
        public string InOut { get; set; }
        public string AdjustedInOut { get; set; }
        public Nullable<long> OperatorId { get; set; }
        public string Note { get; set; }
        public string ClockingType { get; set; }
        public string AdjustedClockingType { get; set; }
        public bool IsDuplicate { get; set; }
        public long OrganizationalUnitDeviceId { get; set; }
        public string AdjustedNote { get; set; }
        public long EmployeeId { get; set; }
        public string PostingMethod { get; set; }
        public bool PublicHolidayWorked { get; set; }
        public bool IsSynced { get; set; }
        public Nullable<System.DateTime> SyncDate { get; set; }
        public Nullable<long> LeaveId { get; set; }
    
        public virtual Employee Employee { get; set; }
        public virtual OrganizationalUnitDevice OrganizationalUnitDevice { get; set; }
    }
}
