//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace UnitTestProject1
{
    using System;
    using System.Collections.Generic;
    
    public partial class DeviceLocation
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public DeviceLocation()
        {
            this.OrganizationalUnitDevices = new HashSet<OrganizationalUnitDevice>();
        }
    
        public long Id { get; set; }
        public Nullable<int> TenantId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public long OrganizationUnitId { get; set; }
    
        public virtual AbpOrganizationUnit AbpOrganizationUnit { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OrganizationalUnitDevice> OrganizationalUnitDevices { get; set; }
    }
}
