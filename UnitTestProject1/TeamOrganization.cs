//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace UnitTestProject1
{
    using System;
    using System.Collections.Generic;
    
    public partial class TeamOrganization
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public TeamOrganization()
        {
            this.TeamShifts = new HashSet<TeamShift>();
        }
    
        public long Id { get; set; }
        public Nullable<int> TenantId { get; set; }
        public long TeamId { get; set; }
        public long OrganizationUnitId { get; set; }
        public string Name { get; set; }
    
        public virtual AbpOrganizationUnit AbpOrganizationUnit { get; set; }
        public virtual Team Team { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TeamShift> TeamShifts { get; set; }
    }
}
