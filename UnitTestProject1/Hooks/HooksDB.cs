﻿using AventStack.ExtentReports;
using AventStack.ExtentReports.Gherkin.Model;
using AventStack.ExtentReports.Model;
using AventStack.ExtentReports.Reporter;
using AventStack.ExtentReports.Reporter.Configuration;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using TechTalk.SpecFlow;


namespace UnitTestProject1.Hooks
{
    [Binding]
    public sealed class HooksDB
    {
        private static ExtentTest feature;
        private static ExtentTest scenario;
        private static ExtentReports extent;
        private static ExtentKlovReporter klov;




        [BeforeTestRun]
        public static void InitializeReport()
        {
            var htmlRepoter = new ExtentHtmlReporter(@"C:\Users\Nogcazis\Documents\Downloads\AutomationReport.html");
            htmlRepoter.Config.Theme = AventStack.ExtentReports.Reporter.Configuration.Theme.Standard;

            extent = new AventStack.ExtentReports.ExtentReports();

            klov = new ExtentKlovReporter();

            klov.InitMongoDbConnection("localhost", 27017);

            klov.ProjectName = "SuperVision DataBase Stored procedure Test";

            // URL of the KLOV server
            klov.InitKlovServerConnection("http://localhost:8077");

            klov.ReportName = "Stored procedure Test" + DateTime.Now.ToString();


            extent.AttachReporter(htmlRepoter, klov);
        }

        [AfterTestRun]
        public static void GenerateReport()
        {

            extent.Flush();
        }

        [BeforeFeature]
        public static void BeforeFeature()
        {
            feature = extent.CreateTest<AventStack.ExtentReports.Gherkin.Model.Feature>(FeatureContext.Current.FeatureInfo.Title);

        }

        [AfterStep]
        public void InsertReport()
        {
            var stepType = ScenarioStepContext.Current.StepInfo.StepDefinitionType.ToString();

            PropertyInfo pInfo = typeof(ScenarioContext).GetProperty("ScenarioExecutionStatus", BindingFlags.Instance | BindingFlags.Public);
            MethodInfo getter = pInfo.GetGetMethod(nonPublic: true);
            object TestResult = getter.Invoke(ScenarioContext.Current, null);

            if (ScenarioContext.Current.TestError == null)
            {
                if (stepType == "Given")
                    scenario.CreateNode<Given>(ScenarioStepContext.Current.StepInfo.Text);
                else if (stepType == "When")
                    scenario.CreateNode<When>(ScenarioStepContext.Current.StepInfo.Text);
                else if (stepType == "Then")
                    scenario.CreateNode<Then>(ScenarioStepContext.Current.StepInfo.Text);
                else if (stepType == "And")
                    scenario.CreateNode<And>(ScenarioStepContext.Current.StepInfo.Text);
            }
            else if (ScenarioContext.Current.TestError != null)
            {
                if (stepType == "Given")
                    scenario.CreateNode<Given>(ScenarioStepContext.Current.StepInfo.Text).Fail(ScenarioContext.Current.TestError.Message);
                else if (stepType == "When")
                    scenario.CreateNode<When>(ScenarioStepContext.Current.StepInfo.Text).Fail(ScenarioContext.Current.TestError.Message);
                else if (stepType == "Then")
                    scenario.CreateNode<Then>(ScenarioStepContext.Current.StepInfo.Text).Fail(ScenarioContext.Current.TestError.Message);
            }

            //Pending Status
            if (TestResult.ToString() == "StepDefinitionPending")
            {
                if (stepType == "Given")
                    scenario.CreateNode<Given>(ScenarioStepContext.Current.StepInfo.Text).Skip("Step Definition Pending");
                else if (stepType == "When")
                    scenario.CreateNode<When>(ScenarioStepContext.Current.StepInfo.Text).Skip("Step Definition Pending");
                else if (stepType == "Then")
                    scenario.CreateNode<Then>(ScenarioStepContext.Current.StepInfo.Text).Skip("Step Definition Pending");

            }

        }

        [BeforeScenario]
        public void BeforeScenario()
        {
            //    ChromeOptions option = new ChromeOptions();
            //    option.AddArgument("window-size=1920,1080");
            //    //option.AddArguments("--disable-gpu");
            //    //option.AddArguments("--no-sandbox");
            //    //option.AddArguments("--headless");

            //    new DriverManager().SetUpDriver(new ChromeConfig());
            //    Console.WriteLine("Setup");
            //    _driverHelper.Driver = new ChromeDriver(option);
            scenario = feature.CreateNode<AventStack.ExtentReports.Gherkin.Model.Scenario>(ScenarioContext.Current.ScenarioInfo.Title);
        }

        //[AfterScenario]
        //public void AfterScenario()
        //{

        //    _driverHelper.Driver.Quit();
        //}
    }
}
