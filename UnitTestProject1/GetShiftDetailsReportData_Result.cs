//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace UnitTestProject1
{
    using System;
    
    public partial class GetShiftDetailsReportData_Result
    {
        public int TenantId { get; set; }
        public string TenantName { get; set; }
        public string Description { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public decimal Duration { get; set; }
        public bool IsNightShift { get; set; }
        public bool IsOffShift { get; set; }
        public int AutoLunch { get; set; }
        public string InRuleDescription { get; set; }
        public string OutRuleDescription { get; set; }
    }
}
