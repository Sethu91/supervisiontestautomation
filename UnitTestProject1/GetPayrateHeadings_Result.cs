//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace UnitTestProject1
{
    using System;
    
    public partial class GetPayrateHeadings_Result
    {
        public long Id { get; set; }
        public string Description { get; set; }
        public string Paycode { get; set; }
    }
}
