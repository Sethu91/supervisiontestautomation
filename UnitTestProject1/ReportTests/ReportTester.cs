﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;
using UnitTestProject1.DatabaseHelpers;

namespace UnitTestProject1.ReportTests
{
    [Binding]
    public class ReportTester
    {
        [Given(@"Test the Database")]
        public void GivenTestTheDatabase(Table table)
        {
            dynamic featureData = table.CreateDynamicInstance();
            var helper = new DBHelper();
            var data = helper.Shifter(featureData);
            var assert = data;
            Assert.True(assert);
        }

    }
}
