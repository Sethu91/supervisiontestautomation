﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnitTestProject1.DatabaseHelpers
{
    public class EmployeeModel
    {
        public long Id { get; set; }
        public Nullable<int> TenantId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string IdNumber { get; set; }
        public string PassportNumber { get; set; }
        public Nullable<System.DateTime> DateOfBirth { get; set; }
        public string EmploymentNumber { get; set; }
        public Nullable<System.DateTime> DateOfEngagement { get; set; }
        public Nullable<System.DateTime> DateOfResignation { get; set; }
        public string ContactNumber { get; set; }
        public string Email { get; set; }
        public Nullable<long> UserId { get; set; }
        public Nullable<long> BEEStatusId { get; set; }
        public Nullable<long> GenderId { get; set; }
        public Nullable<long> NationalityId { get; set; }
        public Nullable<long> RaceId { get; set; }
        public Nullable<long> CategoryFiveLink { get; set; }
        public Nullable<long> CategoryFourLink { get; set; }
        public Nullable<long> CategoryOneLink { get; set; }
        public Nullable<long> CategoryThreeLink { get; set; }
        public Nullable<long> CategoryTwoLink { get; set; }
        public string FingerPrintFour { get; set; }
        public bool FingerPrintFourCaptured { get; set; }
        public string FingerPrintOne { get; set; }
        public bool FingerPrintOneCaptured { get; set; }
        public string FingerPrintThree { get; set; }
        public bool FingerPrintThreeCaptured { get; set; }
        public string FingerPrintTwo { get; set; }
        public bool FingerPrintTwoCaptured { get; set; }
        public Nullable<long> FingerFourName { get; set; }
        public Nullable<long> FingerOneName { get; set; }
        public Nullable<long> FingerThreeName { get; set; }
        public Nullable<long> FingerTwoName { get; set; }
        public Nullable<int> AccessCategorieId { get; set; }
        public Nullable<int> TACategoryId { get; set; }
        public Nullable<long> FridayShift { get; set; }
        public Nullable<long> MondayShift { get; set; }
        public Nullable<long> SaturdayShift { get; set; }
        public Nullable<long> SundayShift { get; set; }
        public Nullable<long> ThursdayShift { get; set; }
        public Nullable<long> TuesdayShift { get; set; }
        public Nullable<long> WednesdayShift { get; set; }
        public Nullable<long> DailyShiftFive { get; set; }
        public Nullable<long> DailyShiftFour { get; set; }
        public Nullable<long> DailyShiftOne { get; set; }
        public Nullable<long> DailyShiftSeven { get; set; }
        public Nullable<long> DailyShiftSix { get; set; }
        public Nullable<long> DailyShiftThree { get; set; }
        public Nullable<long> DailyShiftTwo { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
        public bool IsGlobalSelected { get; set; }
        public Nullable<System.DateTime> EnrollDateTime { get; set; }
        public Nullable<long> EnrollOperatorId { get; set; }
        public Nullable<int> Zone { get; set; }
        public Nullable<System.DateTime> DeletedDateTime { get; set; }
        public Nullable<long> DeletedOperatorID { get; set; }
        public Nullable<System.DateTime> InactiveDateTime { get; set; }
        public Nullable<long> InactiveOperatorID { get; set; }

        public virtual AbpUser AbpUser { get; set; }
        public virtual AbpUser AbpUser1 { get; set; }
        public virtual AbpUser AbpUser2 { get; set; }
        public virtual AccessCategory AccessCategory { get; set; }
        public virtual BEEStatus BEEStatus { get; set; }
        public virtual CategoryFive CategoryFive { get; set; }
        public virtual CategoryFour CategoryFour { get; set; }
        public virtual CategoryOne CategoryOne { get; set; }
        public virtual CategoryThree CategoryThree { get; set; }
        public virtual CategoryTwo CategoryTwo { get; set; }
    }
}
