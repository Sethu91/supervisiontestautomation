﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnitTestProject1.DatabaseHelpers
{
    public class ShiftModel
    {
        public long Id { get; set; }
        public Nullable<int> TenantId { get; set; }
        public decimal Duration { get; set; }
        public string EndTime { get; set; }
        public string StartTime { get; set; }
        public string Description { get; set; }
        public int AdminBreakMinutes { get; set; }
        public bool AllowPublicHolidayOvertime { get; set; }
        public int AutoLunchAfterLeaveInMinutes { get; set; }
        public int AutoLunchInMinute { get; set; }
        public int BonusTimeInMinutes { get; set; }
        public int ClockInWindowMinutes { get; set; }
        public int ClockOutWindowMinutes { get; set; }
        public int DeductLunchAfterInMinutes { get; set; }
        public int EndRoundOption { get; set; }
        public int EndRoundToMin { get; set; }
        public int EndRulesRoundOutClockingsOption { get; set; }
        public int InBetweenRoundOption { get; set; }
        public bool IsAutoLunch { get; set; }
        public bool IsAutoLunchForLeave { get; set; }
        public bool IsBonusTime { get; set; }
        public bool IsClockInWindow { get; set; }
        public bool IsClockOutWindow { get; set; }
        public bool IsDeductLunchDefined { get; set; }
        public bool IsEarlyClockStartTime { get; set; }
        public bool IsFlexiShift { get; set; }
        public bool IsLateShiftClockOutAsEndTime { get; set; }
        public bool IsNightShift { get; set; }
        public bool IsOffShift { get; set; }
        public bool IsRoundClockIn { get; set; }
        public bool IsRoundInBetween { get; set; }
        public bool IsRoundOutClockings { get; set; }
        public bool IsShiftAllowance { get; set; }
        public bool IsShiftTolerance { get; set; }
        public bool IsStandByShift { get; set; }
        public Nullable<long> LeaveAllocationPayCode { get; set; }
        public string LeaveAllowcationName { get; set; }
        public int LunchBreakMinutes { get; set; }
        public decimal MaximunOvertime { get; set; }
        public Nullable<long> OvertimeAfterMaximumPaycode { get; set; }
        public Nullable<long> OvertimePaycode { get; set; }
        public Nullable<long> PublicOvertimeHolidayPaycode { get; set; }
        public int RoundAmountToMin { get; set; }
        public int RoundInBetweenToMin { get; set; }
        public int RoundOutClockMin { get; set; }
        public string ShiftAllocationAllowanceName { get; set; }
        public int ShiftAllowancePercentage { get; set; }
        public Nullable<long> ShiftAllowcationPaycode { get; set; }
        public int ShiftEndClockingOption { get; set; }
        public int ShiftEndWindowMinutes { get; set; }
        public int ShiftStartClockingOption { get; set; }
        public int ShiftStartToleranceAfter { get; set; }
        public int ShiftStartToleranceBefore { get; set; }
        public int ShiftStartWindowMinutes { get; set; }
        public int ShiftToleranceBeforeShiftStartMinutes { get; set; }
        public int SmokeBreakMinutes { get; set; }
        public int StandByShiftPercentage { get; set; }
        public int StartClockRoundingOption { get; set; }
        public int StartRoundOption { get; set; }
        public int StartRoundToMin { get; set; }
        public int ToiletBreakMinutes { get; set; }
        public Nullable<long> MainPayRate { get; set; }
        public Nullable<long> PublicHolidayPaycode { get; set; }
    }
}
