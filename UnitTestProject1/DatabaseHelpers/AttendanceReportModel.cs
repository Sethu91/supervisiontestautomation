﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnitTestProject1.DatabaseHelpers
{
    public class AttendanceReportModel
    {
        public Nullable<int> TenantId { get; set; }
        public DateTime Date { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Device { get; set; }
        public string EmploymentNumber { get; set; }
        public System.DateTime AdjustedClockingDateTime { get; set; }
        public System.DateTime ClockingDateTime { get; set; }
        public Nullable<System.DateTime> SyncDate { get; set; }

    }
}
