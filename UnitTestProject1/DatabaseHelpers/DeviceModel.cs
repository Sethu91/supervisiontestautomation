﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnitTestProject1.DatabaseHelpers
{
    public class DeviceModel
    {
        public long Id { get; set; }
        public Nullable<int> TenantId { get; set; }
        public Nullable<long> DeviceId { get; set; }
        public Nullable<long> DeviceLocationId { get; set; }
        public string CurrentStatus { get; set; }
        public Nullable<System.DateTime> DecommissionDate { get; set; }
        public bool IsActive { get; set; }
        public System.DateTime LinkDate { get; set; }
        public string SerialNumber { get; set; }
        public Nullable<System.DateTime> NextMaintenanceDate { get; set; }
        public Nullable<System.DateTime> PreviousMaintenanceDate { get; set; }
        public string PhysicalLocationName { get; set; }
        public string IpAddress { get; set; }
        public string Area_From { get; set; }
        public string Area_To { get; set; }
        public int Device_Priority { get; set; }
        public string Direction { get; set; }
        public bool EnableAntiPassback { get; set; }
        public bool EnableAreas { get; set; }
        public bool Has_CardReader { get; set; }
        public bool Has_Keyboard { get; set; }
        public bool Has_Screen { get; set; }
        public bool Is_Enrollment { get; set; }
        public bool Is_TNA { get; set; }
        public string Version { get; set; }
        public string UserStorageSize { get; set; }
        public string Name { get; set; }
        public Nullable<System.DateTime> LastSyncDate { get; set; }
        public string Password { get; set; }
        public string Port { get; set; }
        public string UserName { get; set; }
    }
}
