﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Data;
using MySql.Data.MySqlClient;

namespace UnitTestProject1.DatabaseHelpers
{
    public class DBHelper
    {
        public DBHelper()
        { }

        public string DBConn = ConfigurationManager.AppSettings["SuperVisionStoredProcEntities"];

        public SqlTransaction sqltransc;

        //public SqlConnection sqlConnnection;//= new SqlConnection(DBConnection);

        public SqlCommand sqlcommand;// = new SqlCommand("", SQLCon);

        public SqlDataReader sqlreader;

        //static TestContext testContext;

        //[ClassInitialize]
        //public static void ClassInitialize(TestContext context)
        //{
        //    testContext = context;
        //}
        [DataSource("System.Data.SqlClient",
         "Server=.\\SQLExpress; Database=SuperVisionZeroTestDb;User Id=SuperVisionSA; Password=hanniballectoR#1*",
         "Shift", DataAccessMethod.Sequential)]
        public bool Shifter(dynamic data)
        {
            bool result = false;
            DateTime dateTime = new DateTime(2021, 11, 02);
            using (SuperVisionZeroTestDbEntities dbEntities = new SuperVisionZeroTestDbEntities())
            {
                var employees = new List<EmployeeModel>();
                foreach (var item in dbEntities.Employees)
                {
                    var emp = new EmployeeModel();
                    emp.Id = item.Id;
                    emp.FirstName = item.FirstName;
                    emp.LastName = item.LastName;
                    emp.EmploymentNumber = item.EmploymentNumber;

                    employees.Add(emp);
                }
                var devices = new List<OrganizationalUnitDevice>();
                foreach (var item in dbEntities.OrganizationalUnitDevices)
                {
                    var device = new OrganizationalUnitDevice();
                    device.Name = item.Name;
                    device.Id = item.Id;


                    devices.Add(device);
                }
                var clockings = new List<ClockingModel>();
                foreach (var item in dbEntities.Clockings)
                {
                    var clocking = new ClockingModel();
                    clocking.TenantId = item.TenantId;
                    clocking.EmployeeId = item.EmployeeId;
                    clocking.ClockingDateTime = item.ClockingDateTime;
                    clocking.AdjustedClockingDateTime = item.AdjustedClockingDateTime;
                    clocking.SyncDate = item.SyncDate;
                    clocking.OrganizationalUnitDeviceId = item.OrganizationalUnitDeviceId;

                    clockings.Add(clocking);
                }
                var report = new List<AttendanceReportModel>();

                foreach (var item in clockings)
                {
                    var reporter = new AttendanceReportModel();
                    reporter.TenantId = item.TenantId;
                    reporter.FirstName = employees.Where(x => x.Id.Equals(item.EmployeeId)).Select(x => x.FirstName).FirstOrDefault().ToString();
                    reporter.LastName = employees.Where(x => x.Id.Equals(item.EmployeeId)).Select(x => x.LastName).FirstOrDefault().ToString();
                    reporter.EmploymentNumber = employees.Where(x => x.Id.Equals(item.EmployeeId)).Select(x => x.EmploymentNumber).FirstOrDefault().ToString();
                    reporter.ClockingDateTime = item.ClockingDateTime;
                    reporter.AdjustedClockingDateTime = item.AdjustedClockingDateTime;
                    reporter.Device = devices.Where(x => x.Id == item.OrganizationalUnitDeviceId).Select(x => x.Name).FirstOrDefault().ToString();
                    report.Add(reporter);

                }

                var queryReport = report.Where(x => x.TenantId == data.Tenancy && x.ClockingDateTime >= Convert.ToDateTime(data.StartTime)).ToList();

                SqlConnection sqlConnection = new SqlConnection(@"Data Source=10.0.130.7;Initial Catalog=SuperVisionZeroTestDb;Persist Security Info=True;User ID=SuperVisionSA;Password=hanniballectoR#1");
                string sqlQuery = "EXEC [dbo].[GetAttendanceReportData]@TenantId = "+ data.Tenancy + ",@StartRange = '" + Convert.ToDateTime(data.StartTime) + "',@EndRange = '" + Convert.ToDateTime(data.EndTime)+"',@ClockingType = 0 ,@GlobalEmployeeSelect = 0";
                SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(sqlQuery, sqlConnection);
                DataSet dataSet = new DataSet();
                sqlDataAdapter.Fill(dataSet);

                for (int x = 0; x < queryReport.Count; x++)
                {
                    var reporterStoreProc = new AttendanceReportModel();
                    for (int i = 0; i < dataSet.Tables.Count; i++)
                    {
                        reporterStoreProc.FirstName = dataSet.Tables[i].Rows[0]["FirstName"].ToString();
                        reporterStoreProc.LastName = dataSet.Tables[i].Rows[0]["LastName"].ToString();
                        reporterStoreProc.EmploymentNumber = dataSet.Tables[i].Rows[0]["EmploymentNumber"].ToString();
                        reporterStoreProc.AdjustedClockingDateTime = Convert.ToDateTime(dataSet.Tables[i].Rows[0]["InClocking"]);
                        reporterStoreProc.Device = dataSet.Tables[i].Rows[0]["DeviceName"].ToString();

                    }
                    result = queryReport[x].FirstName.ToString() == reporterStoreProc.FirstName;
                    result = queryReport[x].LastName.ToString() == reporterStoreProc.LastName;
                    result = queryReport[x].EmploymentNumber.ToString() == reporterStoreProc.EmploymentNumber;
                }

            }
            
            return result;
        }
    }
}
