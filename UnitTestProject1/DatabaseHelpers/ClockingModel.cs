﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnitTestProject1.DatabaseHelpers
{
    public class ClockingModel
    {
        public long Id { get; set; }
        public Nullable<int> TenantId { get; set; }
        public System.DateTime ClockingDateTime { get; set; }
        public System.DateTime AdjustedClockingDateTime { get; set; }
        public string InOut { get; set; }
        public string AdjustedInOut { get; set; }
        public Nullable<long> OperatorId { get; set; }
        public string Note { get; set; }
        public string ClockingType { get; set; }
        public string AdjustedClockingType { get; set; }
        public bool IsDuplicate { get; set; }
        public long OrganizationalUnitDeviceId { get; set; }
        public string AdjustedNote { get; set; }
        public long EmployeeId { get; set; }
        public string PostingMethod { get; set; }
        public bool PublicHolidayWorked { get; set; }
        public bool IsSynced { get; set; }
        public Nullable<System.DateTime> SyncDate { get; set; }
        public Nullable<long> LeaveId { get; set; }

    }
}
