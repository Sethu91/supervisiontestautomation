﻿Feature: SuperVisionNegative
	Simple calculator for adding two numbers

@mytag
Scenario: Shift  Day Shift 07:00 - 15:00 (rotational) for new employee
	Given Logs in as CPE Tenant
		| UserName | Password   | tenancyName |
		| Sipho    | Fourier123 | TBD         |
	And Navigate to Shift page
	And Add New Shift negative
		| Description         | Starttime | Endtime | Shiftduration | logEarlyClockings | optionsSr        | optionsEr        | clockOutWindow | clockOutWindowMinutes | Normaltime | Overtime | Leaveallocation | Publicholiday  | Allowpublicholidayovertime | Publicholidayovertime |
		| DS 07:00 - 15:00 RT | 07:00     | 15:00   | 8             | true              | Actual Clockings | Actual Clockings | true           | 15                    | NT - Day   | OT - Day | Annual Leave    | Public Holiday | true                       | PH OT                 |


Scenario: Adding a new employee to SuperVision without T&A Catergory
	Given Logs in as CPE Tenant
		| UserName | Password   | tenancyName |
		| Sipho    | Fourier123 | TBD         |
	And And Navigate to Employee Onboarding
	And Add New Employee Negative
		| FirstName | LastName | EmployeeNumber | IdNumber      | DateOfBirth | Gender | Race      | PassportNumber | ContactNumber | Email            | DateOfemployment | DateOfResignation | Nationality   | BEEStatus |
		| Salimna   | Naidoo   | 2020054175     | 9102015856084 | 01031992    | Male   | Caucasian | 9102015856084  | 0798452562    | blackj@gmail.com | 02/01/2018       | 02/01/2021        | South African | Level 1   |
	

Scenario: Adding a new employee to SuperVision with Duplicate Employee Number
	Given Logs in as CPE Tenant
		| UserName | Password   | tenancyName |
		| Sipho    | Fourier123 | TBD         |
	And And Navigate to Employee Onboarding
	And Add New Employee Duplicate Employee Number
		| FirstName | LastName | EmployeeNumber | IdNumber      | DateOfBirth | Gender | Race      | PassportNumber | ContactNumber | Email             | DateOfemployment | DateOfResignation | Nationality   | BEEStatus | accessCatergoryElement | TandACategory |
		| Richard   | Jackel   | cc453          | 9002015856084 | 02011990    | Male   | Caucasian | 9002015856084  | 0798452562    | jackelr@gmail.com | 02012018         | 02012021          | South African | Level 1   | WareHouse              | Rotation 1    |


Scenario: Adding a new employee to SuperVision with incorrect formates
	Given Logs in as CPE Tenant
		| UserName | Password   | tenancyName |
		| Sipho    | Fourier123 | TBD         |
	And And Navigate to Employee Onboarding
	And Add New Employee with incorrect formates
		| FirstName | LastName | EmployeeNumber | IdNumber      | DateOfBirth |  PassportNumber | ContactNumber | Email             | DateOfemployment | DateOfResignation | Nationality   | BEEStatus | accessCatergoryElement | TandACategory |
		| jj45435   | Jackel   | cc453          | ;g0;2h1586084 | 020ghjf     |   900201rbw58   | 0798452562    | jackelr@gmail.com | 02012018         | 02012021          | South African | Level 1   | WareHouse              | Rotation 1    |
