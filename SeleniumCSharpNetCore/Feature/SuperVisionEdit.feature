﻿Feature: SuperVisionEdit
	Check if login functionality works

@mytag
Scenario: Shift  Day Shift 07:00 - 15:00 (rotational) for new employee
	Given Logs in as CPE Tenant
		| UserName | Password   | tenancyName |
		| Sipho    | Fourier123 | TBD         |
	And And Navigate to Shift page
	And Add New Shift
		| Description         | Starttime | Endtime | Shiftduration | logEarlyClockings | optionsSr       | optionsEr       | clockOutWindow | clockOutWindowMinutes | Normaltime | Overtime | Leaveallocation | Publicholiday  | Allowpublicholidayovertime | Publicholidayovertime |
		| DS 07:00 - 17:00 RT | 07        | 17      | 8             | true              | Actual Clocking | Actual Clocking | true           | 15                    | NT - Day   | OT - Day | Annual Leave    | Public Holiday | true                       | PH OT                 |

Scenario: Shift Day Shift Saturday for new employee
	Given Logs in as CPE Tenant
		| UserName | Password   | tenancyName |
		| Sipho    | Fourier123 | TBD         |
	And And Navigate to Shift page
	And Add New Shift
		| Description | Starttime | Endtime | Shiftduration | logEarlyClockings | optionsSr       | optionsEr       | clockOutWindow | clockOutWindowMinutes | Normaltime     | Overtime       | Leaveallocation | Publicholiday  | Allowpublicholidayovertime |
		| Day Saturday | 07        | 13      | 7.5           | true              | Actual Clocking | Actual Clocking | true           | 15                    | NT - Day - Sat | NT - Day - Sat | Annual Leave    | Public Holiday | false                      |

Scenario: Shift Day Shift Sunday for new employee
	Given Logs in as CPE Tenant
		| UserName | Password   | tenancyName |
		| Sipho    | Fourier123 | TBD         |
	And And Navigate to Shift page
	And Add New Shift
		| Description | Starttime | Endtime | Shiftduration | logEarlyClockings | optionsSr       | optionsEr       | clockOutWindow | clockOutWindowMinutes | Normaltime | Overtime | Leaveallocation | Publicholiday  | Allowpublicholidayovertime |
		| Day Sunday   | 07        | 15      | 8             | true              | Actual Clocking | Actual Clocking | true           | 15                    | NT - Day   | OT - Day | Annual Leave    | Public Holiday | false                      |

Scenario: Adding a new employee to SuperVision while logged in as a CPE role
	Given Logs in as CPE Tenant
		| UserName | Password   | tenancyName |
		| Sipho    | Fourier123 | TBD         |
	And And Navigate to Employee Onboarding
	And Add New Employee
		| FirstName | LastName | EmployeeNumber | IdNumber      | DateOfBirth | Gender | Race      | PassportNumber | ContactNumber | Email             | DateOfemployment | DateOfResignation | Nationality   | BEEStatus | accessCatergoryElement | TandACategory |
		| Bruce   | Wayne   | cx4o3MK          | 9002015856084 | 02011990    | Male   | Caucasian | 9002015856084  | 0798452562    | batman@gmail.com | 02012018         | 02012021          | South African | Level 1   | WareHouse              | Rotation 1    |

Scenario: Create shift RollOver for new employee
	Given Logs in as CPE Tenant
		| UserName | Password   | tenancyName |
		| Sipho    | Fourier123 | TBD         |
	And And Navigate to Shift Rollover page
	And Add New ShiftRollover
		| employeeSearch | cycle          | startDate | timeToRoll |
		| Bruce        | Rotation 1     | 02082021  | 0          |

Scenario: Clockings
	Given Logs in as CPE Tenant
		| UserName | Password   | tenancyName |
		| Sipho    | Fourier123 | TBD         |
	And And Navigate to clocking page
	And Add New Clocking
		| Employee | Device    | ClockingDate | Clockingtime | Direction | Clockingtype | Note |
		| Bruce  | HIKVIsion | 2021-09-13   | 07           | IN        | Access       | N/A  |