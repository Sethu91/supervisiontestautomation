﻿Feature: ReportFeatureFile
	Simple calculator for adding two numbers

@mytag
Scenario: Generate Attandence Report
	Given Logs in as CPE Tenant
		| UserName | Password   | tenancyName |
		| Sipho    | Fourier123 | TBD         |
	And And Navigate to Report
	And Genarate Attandence Report
		| FirstName | LastName | EmployeeNumber | IdNumber      | DateOfBirth |  PassportNumber | ContactNumber | Email             | DateOfemployment | DateOfResignation | Nationality   | BEEStatus | accessCatergoryElement | TandACategory |
		| jj45435   | Jackel   | cc453          | ;g0;2h1586084 | 020ghjf     |   900201rbw58   | 0798452562    | jackelr@gmail.com | 02012018         | 02012021          | South African | Level 1   | WareHouse              | Rotation 1    |


		Scenario: Generate Odd Clockings Report
	Given Logs in as CPE Tenant
		| UserName | Password   | tenancyName |
		| Sipho    | Fourier123 | TBD         |
	And And Navigate to Report
	And Genarate Odd Clockings Report
		| FirstName | LastName | EmployeeNumber | IdNumber      | DateOfBirth |  PassportNumber | ContactNumber | Email             | DateOfemployment | DateOfResignation | Nationality   | BEEStatus | accessCatergoryElement | TandACategory |
		| jj45435   | Jackel   | cc453          | ;g0;2h1586084 | 020ghjf     |   900201rbw58   | 0798452562    | jackelr@gmail.com | 02012018         | 02012021          | South African | Level 1   | WareHouse              | Rotation 1    |


		Scenario: Generate Late Arrival Report
	Given Logs in as CPE Tenant
		| UserName | Password   | tenancyName |
		| Sipho    | Fourier123 | TBD         |
	And And Navigate to Report
	And Genarate Late Arrival Report
		| FirstName | LastName | EmployeeNumber | IdNumber      | DateOfBirth |  PassportNumber | ContactNumber | Email             | DateOfemployment | DateOfResignation | Nationality   | BEEStatus | accessCatergoryElement | TandACategory |
		| jj45435   | Jackel   | cc453          | ;g0;2h1586084 | 020ghjf     |   900201rbw58   | 0798452562    | jackelr@gmail.com | 02012018         | 02012021          | South African | Level 1   | WareHouse              | Rotation 1    |


		Scenario: Generate Early Departure Report
	Given Logs in as CPE Tenant
		| UserName | Password   | tenancyName |
		| Sipho    | Fourier123 | TBD         |
	And And Navigate to Report
	And Genarate Early Departure Report
		| FirstName | LastName | EmployeeNumber | IdNumber      | DateOfBirth |  PassportNumber | ContactNumber | Email             | DateOfemployment | DateOfResignation | Nationality   | BEEStatus | accessCatergoryElement | TandACategory |
		| jj45435   | Jackel   | cc453          | ;g0;2h1586084 | 020ghjf     |   900201rbw58   | 0798452562    | jackelr@gmail.com | 02012018         | 02012021          | South African | Level 1   | WareHouse              | Rotation 1    |




		Scenario: Generate Exception Management Report
	Given Logs in as CPE Tenant
		| UserName | Password   | tenancyName |
		| Sipho    | Fourier123 | TBD         |
	And And Navigate to Report
	And Genarate Exception Management Report
		| FirstName | LastName | EmployeeNumber | IdNumber      | DateOfBirth |  PassportNumber | ContactNumber | Email             | DateOfemployment | DateOfResignation | Nationality   | BEEStatus | accessCatergoryElement | TandACategory |
		| jj45435   | Jackel   | cc453          | ;g0;2h1586084 | 020ghjf     |   900201rbw58   | 0798452562    | jackelr@gmail.com | 02012018         | 02012021          | South African | Level 1   | WareHouse              | Rotation 1    |

