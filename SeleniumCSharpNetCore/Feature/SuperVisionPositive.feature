﻿Feature: SuperVisionPositive
	Check if login functionality works

@mytag
Scenario: Pay Rate  NT-Day
	Given Logs in as CPE Tenant
		| UserName | Password   | tenancyName |
		| Thapelo  | Fourier123 | TBD         |
	And Navigates to Pay Rates
	And Add New PayRate
		| Description | Rate | Paycode |
		| Weekend     | 1.0  | 1000    |

Scenario: Pay Rate  OT-Day
	Given Logs in as CPE Tenant
		| UserName | Password   | tenancyName |
		| Thapelo  | Fourier123 | TBD         |
	And Navigates to Pay Rates
	And Add New PayRate
		| Description | Rate | Paycode |
		| Overtime    | 1.5  | 1001    |

Scenario: Pay Rate  NT-Day-Sat
	Given Logs in as CPE Tenant
		| UserName | Password   | tenancyName |
		| Thapelo  | Fourier123 | TBD         |
	And Navigates to Pay Rates
	And Add New PayRate
		| Description | Rate | Paycode |
		| Day-Sat     | 1.0  | 1006    |

Scenario: Pay Rate  Public Holiday
	Given Logs in as CPE Tenant
		| UserName | Password   | tenancyName |
		| Thapelo  | Fourier123 | TBD         |
	And Navigates to Pay Rates
	And Add New PayRate
		| Description    | Rate | Paycode |
		| Public Holiday | 1.0  | 1222    |

Scenario: Pay Rate  PH-OT
	Given Logs in as CPE Tenant
		| UserName | Password   | tenancyName |
		| Thapelo  | Fourier123 | TBD         |
	And Navigates to Pay Rates
	And Add New PayRate
		| Description | Rate | Paycode |
		| PH-Overtime | 1.0  | 1223    |

Scenario: Work Cycle Rotation 1
	Given Logs in as CPE Tenant
		| UserName | Password   | tenancyName |
		| Thapelo  | Fourier123 | TBD         |
	And Navigates to Work Cycles
	And Add New WorkCycle
		| Description | Shifts |
		| Rotation 1  | 1.0    |

Scenario: T&A Category Rotation 1
	Given Logs in as CPE Tenant
		| UserName | Password   | tenancyName |
		| Thapelo  | Fourier123 | TBD         |
	And Navigates to T&A Categories
	And Add New T&A Category
		| Description | Shift Type  | Reporting Method |
		| Rotation 1  | Shift Cycle | All Clockings    |

Scenario: Apply TA Rules for new employee
	Given Logs in as CPE Tenant
		| UserName | Password   | tenancyName |
		| Thapelo  | Fourier123 | TBD         |
	And Navigates to Apply T&A Rules
	And Add New T&A Rule
		| employeeSearch | startDate | endDate  |
		| Bruce          | 02082021  | 02092021 |

Scenario: Shift  Day Shift 07:00 - 15:00 (rotational) for new employee
	Given Logs in as CPE Tenant
		| UserName | Password   | tenancyName |
		| Sipho    | Fourier123 | TBD         |
	And And Navigate to Shift page
	And Add New Shift
		| Description         | Starttime | Endtime | Shiftduration | logEarlyClockings | optionsSr       | optionsEr       | clockOutWindow | clockOutWindowMinutes | Normaltime | Overtime | Leaveallocation | Publicholiday  | Allowpublicholidayovertime | Publicholidayovertime |
		| DS 07:00 - 17:00 RT | 07        | 17      | 8             | true              | Actual Clocking | Actual Clocking | true           | 15                    | NT - Day   | OT - Day | Annual Leave    | Public Holiday | true                       | PH OT                 |

Scenario: Shift Day Shift Saturday for new employee
	Given Logs in as CPE Tenant
		| UserName | Password   | tenancyName |
		| Sipho    | Fourier123 | TBD         |
	And And Navigate to Shift page
	And Add New Shift
		| Description  | Starttime | Endtime | Shiftduration | logEarlyClockings | optionsSr       | optionsEr       | clockOutWindow | clockOutWindowMinutes | Normaltime     | Overtime       | Leaveallocation | Publicholiday  | Allowpublicholidayovertime |
		| Day Saturday | 07        | 13      | 7.5           | true              | Actual Clocking | Actual Clocking | true           | 15                    | NT - Day - Sat | NT - Day - Sat | Annual Leave    | Public Holiday | false                      |

Scenario: Shift Day Shift Sunday for new employee
	Given Logs in as CPE Tenant
		| UserName | Password   | tenancyName |
		| Sipho    | Fourier123 | TBD         |
	And And Navigate to Shift page
	And Add New Shift
		| Description | Starttime | Endtime | Shiftduration | logEarlyClockings | optionsSr       | optionsEr       | clockOutWindow | clockOutWindowMinutes | Normaltime | Overtime | Leaveallocation | Publicholiday  | Allowpublicholidayovertime |
		| Day Sunday  | 07        | 15      | 8             | true              | Actual Clocking | Actual Clocking | true           | 15                    | NT - Day   | OT - Day | Annual Leave    | Public Holiday | false                      |

Scenario: Adding a new employee to SuperVision while logged in as a CPE role
	Given Logs in as CPE Tenant
		| UserName | Password   | tenancyName |
		| Sipho    | Fourier123 | TBD         |
	And And Navigate to Employee Onboarding
	And Add New Employee
		| FirstName | LastName | EmployeeNumber | IdNumber      | DateOfBirth | Gender | Race      | PassportNumber | ContactNumber | Email            | DateOfemployment | DateOfResignation | Nationality   | BEEStatus | accessCatergoryElement | TandACategory |
		| Jack      | Black    | CP0562555      | 9102015856084 | 02011990    | Male   | Caucasian | 9002015856084  | 0798452562    | batman@gmail.com | 02012018         | 02012021          | South African | Level 1   | WareHouse              | Rotation 1    |

Scenario: Create shift RollOver for new employee
	Given Logs in as CPE Tenant
		| UserName | Password   | tenancyName |
		| Sipho    | Fourier123 | TBD         |
	And And Navigate to Shift Rollover page
	And Add New ShiftRollover
		| employeeSearch | cycle      | startDate | timeToRoll |
		| Bruce          | Rotation 1 | 02082021  | 5          |

Scenario: Clockings
	Given Logs in as CPE Tenant
		| UserName | Password   | tenancyName |
		| Sipho    | Fourier123 | TBD         |
	And And Navigate to clocking page
	And Add New Clocking
		| Employee | Device    | ClockingDate | Clockingtime | Direction | Clockingtype | Note |
		| Bruce    | HIKVIsion | 2021-09-13   | 07           | IN        | Access       | N/A  |

Scenario: Adding a new employee to SuperVision while logged in as a CPE role Coypu
	Given Logs in as CPE Tenant Coypu
		| UserName | Password   | tenancyName |
		| Sipho    | Fourier123 | TBD         |
	And And Navigate to Employee Onboarding Coypu
	And Add New Employee Coypu
		| FirstName | LastName | EmployeeNumber | IdNumber      | DateOfBirth | Gender | Race      | PassportNumber | ContactNumber | Email            | DateOfemployment | DateOfResignation | Nationality   | BEEStatus | accessCatergoryElement | TandACategory |
		| Bruce     | Wayne    | cx4o3MK#       | 9002015856084 | 02011990    | Male   | Caucasian | 9002015856084  | 0798452562    | batman@gmail.com | 02012018         | 02012021          | South African | Level 1   | WareHouse              | Sipho 2   |

Scenario: Shift Copy
	Given Logs in as CPE Tenant
		| UserName | Password   | tenancyName |
		| Sipho    | Fourier123 | TBD         |
	And And Navigate to Shift copy page
	And Add Shift Copy
		| EmployeeToCopyFrom        | EmployeeToCopyTo       | FromDate   | ToDate     |
		| Wayne, Bruce - cx4o3MK#12 | Jack Black - CP0562555 | 05/13/2022 | 05/19/2022 |