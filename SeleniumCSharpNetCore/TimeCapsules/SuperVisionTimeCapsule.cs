﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Text;

namespace SuperViosionAutomationTester.TimeCapsules
{
    public class SuperVisionTimeCapsule
    {
        TimeSpan timeOut = new TimeSpan(0, 0, 25, 0, 0);
        private IWebDriver Driver;
        public SuperVisionTimeCapsule(IWebDriver driver)
        {
            Driver = driver;
        }
        public void GetTimeClickstartTimeShift(string data) 
        {
            WebDriverWait wait = new WebDriverWait(Driver, timeOut);
            var startTime = wait.Until(ExpectedConditions.ElementIsVisible(By.CssSelector("#Shift_StartTime .ngb-tp-hour > .btn:nth-child(1)")));
            string hours = data;

            switch (hours)
            {
                case ("1"):
                    startTime.Click();
                    break;
                case ("2"):
                    startTime.Click(); startTime.Click();
                    break;
                case ("3"):
                    startTime.Click(); startTime.Click(); startTime.Click();
                    break;
                case ("4"):
                    startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click();
                    break;
                case ("5"):
                    startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click();
                    break;
                case ("6"):
                    startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click();
                    break;
                case ("7"):
                    startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click();
                    break;
                case ("8"):
                    startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click();
                    break;
                case ("9"):
                    startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click();
                    break;
                case ("10"):
                    startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click();
                    break;
                case ("11"):
                    startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click();
                    break;
                case ("12"):
                    startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click();
                    break;
                case ("13"):
                    startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click();
                    break;
                case ("14"):
                    startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click();
                    break;
                case ("15"):
                    startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click();
                    break;
                case ("16"):
                    startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click();
                    break;
                case ("17"):
                    startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click();
                    break;
                case ("18"):
                    startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click();
                    break;
                case ("19"):
                    startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click();
                    break;
                case ("20"):
                    startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click();
                    break;
                case ("21"):
                    startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click();
                    break;
                case ("22"):
                    startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click();
                    break;
                case ("23"):
                    startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click(); startTime.Click();
                    break;
                default:
                    break;
            }
        }
        public void GetTimeClickendTimeShift(string data) 
        {
            WebDriverWait wait = new WebDriverWait(Driver, timeOut);
            var endTime = wait.Until(ExpectedConditions.ElementIsVisible(By.CssSelector("#Shift_EndTime .ngb-tp-hour > .btn:nth-child(1)")));
            string hours = data;

            switch (hours)
            {
                case ("1"):
                    endTime.Click();
                    break;
                case ("2"):
                    endTime.Click(); endTime.Click();
                    break;
                case ("3"):
                    endTime.Click(); endTime.Click(); endTime.Click();
                    break;
                case ("4"):
                    endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click();
                    break;
                case ("5"):
                    endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click();
                    break;
                case ("6"):
                    endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click();
                    break;
                case ("7"):
                    endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click();
                    break;
                case ("8"):
                    endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click();
                    break;
                case ("9"):
                    endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click();
                    break;
                case ("10"):
                    endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click();
                    break;
                case ("11"):
                    endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click();
                    break;
                case ("12"):
                    endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click();
                    break;
                case ("13"):
                    endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click();
                    break;
                case ("14"):
                    endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click();
                    break;
                case ("15"):
                    endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click();
                    break;
                case ("16"):
                    endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click();
                    break;
                case ("17"):
                    endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click();
                    break;
                case ("18"):
                    endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click();
                    break;
                case ("19"):
                    endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click();
                    break;
                case ("20"):
                    endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click();
                    break;
                case ("21"):
                    endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click();
                    break;
                case ("22"):
                    endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click();
                    break;
                case ("23"):
                    endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click(); endTime.Click();
                    break;
                default:
                    break;
            }
        }
        public void GetTimeClickclockingTime(string data) 
        {
            WebDriverWait wait = new WebDriverWait(Driver, timeOut);
            var clockingTime = wait.Until(ExpectedConditions.ElementIsVisible(By.CssSelector(".ngb-tp-hour > .btn:nth-child(1) > .chevron")));
            string hours = data;

            switch (hours)
            {
                case ("1"):
                    clockingTime.Click();
                    break;
                case ("2"):
                    clockingTime.Click(); clockingTime.Click();
                    break;
                case ("3"):
                    clockingTime.Click(); clockingTime.Click(); clockingTime.Click();
                    break;
                case ("4"):
                    clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click();
                    break;
                case ("5"):
                    clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click();
                    break;
                case ("6"):
                    clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click();
                    break;
                case ("7"):
                    clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click();
                    break;
                case ("8"):
                    clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click();
                    break;
                case ("9"):
                    clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click();
                    break;
                case ("10"):
                    clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click();
                    break;
                case ("11"):
                    clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click();
                    break;
                case ("12"):
                    clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click();
                    break;
                case ("13"):
                    clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click();
                    break;
                case ("14"):
                    clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click();
                    break;
                case ("15"):
                    clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click();
                    break;
                case ("16"):
                    clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click();
                    break;
                case ("17"):
                    clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click();
                    break;
                case ("18"):
                    clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click();
                    break;
                case ("19"):
                    clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click();
                    break;
                case ("20"):
                    clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click();
                    break;
                case ("21"):
                    clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click();
                    break;
                case ("22"):
                    clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click();
                    break;
                case ("23"):
                    clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click(); clockingTime.Click();
                    break;
                default:
                    break;
            }
        }
    }
}
