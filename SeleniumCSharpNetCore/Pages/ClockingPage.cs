﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using SuperViosionAutomationTester.TimeCapsules;
using System;
using System.Collections.Generic;
using System.Text;

namespace SuperViosionAutomationTester.Pages
{
    public class ClockingPage
    {
        TimeSpan timeOut = new TimeSpan(0, 0, 25, 0, 0);
        private DriverHelper _driverHelper;
        private IWebDriver Driver;
        SuperVisionTimeCapsule timeCapsulePage;
        public ClockingPage(IWebDriver driver)
        {
            Driver = driver;
            timeCapsulePage = new SuperVisionTimeCapsule(driver);
        }
        public void createClocking(dynamic data)
        {
            WebDriverWait wait = new WebDriverWait(Driver, timeOut);
            var addClocking = wait.Until(ExpectedConditions.ElementIsVisible(By.CssSelector(".btn:nth-child(4)")));
            addClocking.Click();
            //var employee = wait.Until(ExpectedConditions.ElementIsVisible(By.CssSelector(".form-group:nth-child(1) > .input-group > .input-group-append > .btn")));
            //employee.Click();
            //var elementEmployee = wait.Until(ExpectedConditions.ElementIsVisible(By.CssSelector(".btn-group > .btn")));
            //elementEmployee.Click();
            //var device = wait.Until(ExpectedConditions.ElementIsVisible(By.CssSelector(".form-group:nth-child(3) .input-group-append > .btn")));
            //device.Click();
            //var elementDevice = wait.Until(ExpectedConditions.ElementIsVisible(By.CssSelector(".btn-group > .btn")));
            //elementDevice.Click();
            var clockingDate = wait.Until(ExpectedConditions.ElementIsVisible(By.Name("dp")));
            clockingDate.Clear();
            var dateData = data.ClockingDate.ToString("yyyy-MM-dd");
            clockingDate.SendKeys(Convert.ToString(dateData));
            
            timeCapsulePage.GetTimeClickclockingTime(Convert.ToString(data.Clockingtime));

            //var direction = wait.Until(ExpectedConditions.ElementIsVisible(By.Id("checkDefault2")));
            //direction.Click();
            //var clockingtype = wait.Until(ExpectedConditions.ElementIsVisible(By.Id("clockCheckDefault2")));
            //clockingtype.Click();
            //var save = wait.Until(ExpectedConditions.ElementIsVisible(By.CssSelector(".modal-footer > .blue:nth-child(2)")));
            //save.Click();
            
        }

    }
}
