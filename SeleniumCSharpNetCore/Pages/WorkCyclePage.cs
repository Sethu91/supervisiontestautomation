﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Text;

namespace SuperViosionAutomationTester.Pages
{
    public class WorkCyclePage
    {
        TimeSpan timeOut = new TimeSpan(0, 0, 25, 0, 0);
        private IWebDriver Driver;

        public WorkCyclePage(IWebDriver driver)
        {
            Driver = driver;
        }
        public void createWorkCycle(dynamic data)
        {
            WebDriverWait wait = new WebDriverWait(Driver, timeOut);
            var workCycle = wait.Until(ExpectedConditions.ElementIsVisible(By.Id("WorkCycleDescription")));
            workCycle.SendKeys(data.Description);

            var save = wait.Until(ExpectedConditions.ElementIsVisible(By.CssSelector(".blue > span")));
            save.Click();

        }
        public void editWorkCycle(dynamic data)
        {

        }
    }
}
