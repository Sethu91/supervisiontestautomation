﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace SuperViosionAutomationTester.Pages
{
    public class ShiftRollOverPage
    {
        TimeSpan timeOut = new TimeSpan(0, 0, 25, 0, 0);
        private IWebDriver Driver;

        public ShiftRollOverPage(IWebDriver driver)
        {
            Driver = driver;
        }
        public void shiftRollOver(dynamic data)
        {
            WebDriverWait wait = new WebDriverWait(Driver, timeOut);

            
            var employees = wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("//button[contains(@class, 'btn-primary')]")));
            employees.Click();
            //var chooseEmp = wait.Until(ExpectedConditions.ElementIsVisible(By.CssSelector(".ng-select-focused input")));
            //chooseEmp.Click();
            //var chooseEmpElement = wait.Until(ExpectedConditions.ElementIsVisible(By.CssSelector(".ng-select-focused input")));
            //chooseEmpElement.Click();By.CssSelector(".ng-select-focused input")
            //var filter = wait.Until(ExpectedConditions.ElementIsVisible(By.CssSelector(".ng-arrow-wrapper:nth-child(3)")));
            //filter.Click();
            //var filterElement = new SelectElement(filter);
            //filterElement.SelectByText(data.Employee);
            var selectFilter = wait.Until(ExpectedConditions.ElementIsVisible(By.CssSelector(".btn-success:nth-child(2)")));
            selectFilter.Click();
            var close = wait.Until(ExpectedConditions.ElementIsVisible(By.CssSelector(".modal-xl .modal-footer > .btn")));
            close.Click();



            // driver.FindElement(By.CssSelector(".ng-select-focused input")).Click();
            //driver.FindElement(By.CssSelector("#a63e309a4a96-0 > .ng-option-label")).Click();
            //driver.FindElement(By.CssSelector(".btn-success:nth-child(2)")).Click();
            //driver.FindElement(By.CssSelector(".modal-xl .modal-footer > .btn")).Click();

            ///Driver.FindElement(By.XPath("//button[contains(@class, 'btn-primary')]")).Click(); By.XPath("//button[contains(@class, 'btn-primary')]")
            var cycle = wait.Until(ExpectedConditions.ElementIsVisible(By.CssSelector(".ng-tns-c25-3:nth-child(7)")));
            var cycleElement = new SelectElement(cycle);
            Thread.Sleep(2000);
            Thread.Sleep(2000);
            cycleElement.SelectByText(data.cycle);
            var startDate = wait.Until(ExpectedConditions.ElementIsVisible(By.Id("StartRange")));
            startDate.SendKeys(Convert.ToString(data.startDate));
            var timeToRoll = wait.Until(ExpectedConditions.ElementIsVisible(By.Id("TimeToRoll")));
            timeToRoll.SendKeys(Convert.ToString(data.timeToRoll));

            //driver.FindElement(By.CssSelector(".btn > .ng-tns-c25-3")).Click();

            var roll = wait.Until(ExpectedConditions.ElementIsVisible(By.CssSelector(".btn > .ng-tns-c25-3")));
            roll.Click();
            Assert.Multiple(() =>
            {
                Assert.That(roll, Is.Null);
            });
        }
    }
}
