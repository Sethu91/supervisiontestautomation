﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using SuperViosionAutomationTester.Pages;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;

namespace SuperViosionAutomationTester.Pages.DeletePage
{
    public class EmployeeDeletePage
    {
        TimeSpan timeOut = new TimeSpan(0, 0, 25, 0, 0);
        private IWebDriver Driver;


        public EmployeeDeletePage(IWebDriver driver)
        {
            Driver = driver;
        }
        public void deleteEmp(dynamic data) 
        {
            Thread.Sleep(2000);
            Thread.Sleep(2000);
            WebDriverWait wait = new WebDriverWait(Driver, timeOut);
            var searchEmp = wait.Until(ExpectedConditions.ElementIsVisible(By.Name("filterText")));
            searchEmp.SendKeys("Bruce");
            var searchBtn = wait.Until(ExpectedConditions.ElementIsVisible(By.CssSelector(".ng-tns-c25-6 > .row .flaticon-search-1")));
            searchBtn.Click();
            //var deletBtn = wait.Until(ExpectedConditions.ElementIsVisible(By.CssSelector(".kt-menu__item--hover:nth-child(1) .kt-menu__link-text")));
            //deletBtn.Click();
            var delte = wait.Until(ExpectedConditions.ElementIsVisible(By.CssSelector(".ng-tns-c25-6:nth-child(1) > .ng-tns-c25-6 .dropdown-toggle")));
            delte.Click();
            var deleteCnfrm = wait.Until(ExpectedConditions.ElementIsVisible(By.LinkText("Delete")));
            deleteCnfrm.Click();
            var cnDlt = wait.Until(ExpectedConditions.ElementIsVisible(By.CssSelector(".swal2-confirm")));
            cnDlt.Click();

        }
    }
}
