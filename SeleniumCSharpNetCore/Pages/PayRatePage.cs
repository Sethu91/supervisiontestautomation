﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Text;

namespace SuperViosionAutomationTester.Pages
{
    public class PayRatePage
    {
        TimeSpan timeOut = new TimeSpan(0, 0, 25, 0, 0);
        private IWebDriver Driver;

        public PayRatePage(IWebDriver driver)
        {
            Driver = driver;
        }
        public void createPayRate(dynamic data)
        {
            WebDriverWait wait = new WebDriverWait(Driver, timeOut);
            var payrate = wait.Until(ExpectedConditions.ElementIsVisible(By.Id("PayRateDescription")));
            payrate.SendKeys(data.Description);        

           var rate = wait.Until(ExpectedConditions.ElementIsVisible(By.Id("PayRate_Rate")));
            rate.SendKeys(Convert.ToString(data.Rate));

            var paycode = wait.Until(ExpectedConditions.ElementIsVisible(By.Id("PayRate_Paycode")));
            paycode.SendKeys(Convert.ToString(data.Paycode));

            var save = wait.Until(ExpectedConditions.ElementIsVisible(By.CssSelector(".blue > span")));
            save.Click();

        }

    }
}
