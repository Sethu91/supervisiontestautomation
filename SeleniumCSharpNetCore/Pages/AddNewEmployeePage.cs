﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using TechTalk.SpecFlow;

namespace SuperViosionAutomationTester.Pages
{
    public class AddNewEmployeePage
    {
        TimeSpan timeOut = new TimeSpan(0, 0, 25, 0, 0);
        private IWebDriver Driver;

        public AddNewEmployeePage(IWebDriver driver)
        {
            Driver = driver;
        }
        public void addNewEmployee(dynamic data)
        {
            Thread.Sleep(2000);
            Thread.Sleep(2000);
            WebDriverWait wait = new WebDriverWait(Driver, timeOut);
            var txtEmployeeFirstName = wait.Until(ExpectedConditions.ElementIsVisible(By.Id("EmployeeFirstName")));
            txtEmployeeFirstName.SendKeys(data.FirstName);
            Thread.Sleep(2000);
            var txtEmployeeLastName = wait.Until(ExpectedConditions.ElementIsVisible(By.Id("Employee_LastName")));
            txtEmployeeLastName.SendKeys(data.LastName);
            Thread.Sleep(2000);
            var txtEmployeeNumber = wait.Until(ExpectedConditions.ElementIsVisible(By.Id("Employee_EmploymentNumber")));
            txtEmployeeNumber.SendKeys(Convert.ToString(data.EmployeeNumber));
            Thread.Sleep(2000);
            var txtEmployeeIdNumber = wait.Until(ExpectedConditions.ElementIsVisible(By.Id("Employee_IdNumber")));
            txtEmployeeIdNumber.SendKeys(Convert.ToString(data.IdNumber));
            Thread.Sleep(2000);
            var txtEmployeeBirthDate = wait.Until(ExpectedConditions.ElementIsVisible(By.Id("Employee_DateOfBirth")));
            txtEmployeeBirthDate.SendKeys(Convert.ToString(data.DateOfBirth));
            Thread.Sleep(2000);
            var txtEmployeePassportNumber = wait.Until(ExpectedConditions.ElementIsVisible(By.Id("Employee_PassportNumber")));
            txtEmployeePassportNumber.SendKeys(Convert.ToString(data.PassportNumber));
            Thread.Sleep(2000);
            var txtEmployeeContactNumber = wait.Until(ExpectedConditions.ElementIsVisible(By.Id("Employee_ContactNumber")));
            txtEmployeeContactNumber.SendKeys(Convert.ToString(data.ContactNumber));
            Thread.Sleep(2000);
            var txtEmployeeEmail = wait.Until(ExpectedConditions.ElementIsVisible(By.Id("Employee_Email")));
            txtEmployeeEmail.SendKeys(Convert.ToString(data.Email));
            Thread.Sleep(2000);
            var txtEmployeeDateEngagement = wait.Until(ExpectedConditions.ElementIsVisible(By.Id("Employee_DateOfEngagement")));
            txtEmployeeDateEngagement.SendKeys(Convert.ToString(data.DateOfemployment));
            Thread.Sleep(2000);
            var txtEmployeeDateResignation = wait.Until(ExpectedConditions.ElementIsVisible(By.Id("Employee_DateOfResignation")));
            txtEmployeeDateResignation.SendKeys(Convert.ToString(data.DateOfResignation));
            var tab = wait.Until(ExpectedConditions.ElementIsVisible(By.LinkText("Categories")));
            tab.Click();

            #region  accessCatergory
            //var accessCatergory = wait.Until(ExpectedConditions.ElementIsVisible(By.CssSelector(".row:nth-child(1) > .col-md-6:nth-child(1) > .form-group:nth-child(1) .input-group-append > .btn")));
            //accessCatergory.Click();
            //Thread.Sleep(2000);
            #endregion


            var TACatergory = wait.Until(ExpectedConditions.ElementIsVisible(By.CssSelector(".col-md-6:nth-child(1) > .form-group:nth-child(3) .input-group-append > .btn")));
         

            
          

            IJavaScriptExecutor js = (IJavaScriptExecutor)Driver;
            js.ExecuteScript("arguments[0].click();", TACatergory);
            //List < String > checked = Array.asList(new String[] { "user1_admin", "user3_browser" };
            //List <IWebElement> elements = (List<IWebElement>)js.ExecuteScript("return jQuery.find(':button')");
            //ypeText('#empModal .form-control.m-input.ng-valid.ng-dirty.ng-touched', 'sipho')

            //elementTACatergory.SendKeys("Sipho");
            //List<string> allElements = new List<string>();
            Actions builder = new Actions(Driver);
            var elementTACatergory = wait.Until(ExpectedConditions.ElementIsVisible(By.CssSelector(".ng-star-inserted:nth-child(1) > td .btn")));
            builder.MoveToElement(elementTACatergory).Click().Build().Perform();
            js.ExecuteScript("arguments[0].click();", elementTACatergory);




        





            //js.ExecuteScript("document.getElementById(id').value='elementTACatergory';");
           // builder.MoveToElement(elementTACatergory).Click().Build().Perform();
            Thread.Sleep(2000);

            Thread.Sleep(2000);

            var submit = Driver.FindElement(By.XPath("//div[@class ='modal-footer']//child::button[2]"));
            submit.Click();
            Thread.Sleep(2000);
            Driver.FindElement(By.Name("filterText")).SendKeys(data.FirstName);
            Thread.Sleep(2000);
            var search = Driver.FindElement(By.XPath("//span[@class ='input-group-btn']//child::button[1]"));
            search.Click();


        }
    }
}
