﻿using SuperViosionAutomationTester.Steps;
using System;
using System.Collections.Generic;
using System.Text;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;

namespace SuperViosionAutomationTester.Pages
{
    [Binding]
    public class ReportSteps
    {
        private DriverHelper _driverHelper;
        ReportStep reportStep;


        public ReportSteps(DriverHelper driverHelper)
        {
            _driverHelper = driverHelper;
            reportStep = new ReportStep(_driverHelper.Driver);

        }


        [Given(@"Genarate Attandence Report")]
        public void GivenGenarateAttandenceReport(Table table)
        {
            dynamic data = table.CreateDynamicInstance();
            reportStep.attendenceReport(data);
        }

        [Given(@"Genarate Odd Clockings Report")]
        public void GivenGenarateOddClockingsReport(Table table)
        {
            dynamic data = table.CreateDynamicInstance();
            reportStep.attendenceReport(data);
        }

        [Given(@"Genarate Late Arrival Report")]
        public void GivenGenarateLateArrivalReport(Table table)
        {
            dynamic data = table.CreateDynamicInstance();
            reportStep.attendenceReport(data);
        }

        [Given(@"Genarate Early Departure Report")]
        public void GivenGenarateEarlyDepartureReport(Table table)
        {
            dynamic data = table.CreateDynamicInstance();
            reportStep.attendenceReport(data);
        }

        [Given(@"Genarate Exception Management Report")]
        public void GivenGenarateExceptionManagementReport(Table table)
        {
            dynamic data = table.CreateDynamicInstance();
            reportStep.attendenceReport(data);
        }


    }
}
