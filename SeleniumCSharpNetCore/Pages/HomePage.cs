﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace SuperViosionAutomationTester.Pages
{
    public class HomePage
    {
        TimeSpan timeOut = new TimeSpan(0, 0, 25, 0, 0);
        private IWebDriver Driver;

        public HomePage(IWebDriver driver)
        {
            Driver = driver;
        }
        public void PayRates()
        {
            WebDriverWait wait = new WebDriverWait(Driver, timeOut);
            Thread.Sleep(2000);
            var timeAndAttendence = wait.Until(ExpectedConditions.ElementIsVisible(By.LinkText("Time & Attendance")));
            timeAndAttendence.Click();

            var config = wait.Until(ExpectedConditions.ElementIsVisible(By.LinkText("Configuration")));
            config.Click();

            var payrates = wait.Until(ExpectedConditions.ElementIsVisible(By.LinkText("Pay Rates")));
            payrates.Click();
            var addPayRateButton = wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("//button[contains(@class, 'primary')]")));
            addPayRateButton.Click();
        }
        public void WorkCycles()
        {
            WebDriverWait wait = new WebDriverWait(Driver, timeOut);
            Thread.Sleep(2000);
            var timeAndAttendence = wait.Until(ExpectedConditions.ElementIsVisible(By.LinkText("Time & Attendance")));
            timeAndAttendence.Click();

            var config = wait.Until(ExpectedConditions.ElementIsVisible(By.LinkText("Configuration")));
            config.Click();

            var workcycles = wait.Until(ExpectedConditions.ElementIsVisible(By.LinkText("Work Cycles")));
            workcycles.Click();

            var addNewWorkCycleButton = wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("//button[contains(@class, 'primary')]")));
            addNewWorkCycleButton.Click();
        }

        public void TAndACategories()
        {
            WebDriverWait wait = new WebDriverWait(Driver, timeOut);
            Thread.Sleep(2000);
            var timeAndAttendence = wait.Until(ExpectedConditions.ElementIsVisible(By.LinkText("Time & Attendance")));
            timeAndAttendence.Click();

            var config = wait.Until(ExpectedConditions.ElementIsVisible(By.LinkText("Configuration")));
            config.Click();

            var tAndACategories = wait.Until(ExpectedConditions.ElementIsVisible(By.LinkText("T&A Categories")));
            tAndACategories.Click();
            var addTACategoryButton = wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("//button[contains(@class, 'primary')]")));
            addTACategoryButton.Click();
        }
        public void ApplyTAndARules()
        {
            WebDriverWait wait = new WebDriverWait(Driver, timeOut);
            Thread.Sleep(2000);
            var timeAndAttendence = wait.Until(ExpectedConditions.ElementIsVisible(By.LinkText("Time & Attendance")));
            timeAndAttendence.Click();

            var operations = wait.Until(ExpectedConditions.ElementIsVisible(By.LinkText("Operations")));
            operations.Click();

            var applyTAndARules = wait.Until(ExpectedConditions.ElementIsVisible(By.LinkText("Apply T&A Rules")));
            applyTAndARules.Click();


        }

        public void employeeOnBoarding()
        {
            Thread.Sleep(2000);
            WebDriverWait wait = new WebDriverWait(Driver, timeOut);
            Thread.Sleep(2000);
            Thread.Sleep(2000);
            var sidebar = wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("//span[contains(@class, 'text')]")));
            sidebar.Click();
            var employeeBoarding = wait.Until(ExpectedConditions.ElementIsVisible(By.LinkText("Employee on-Boarding")));
            employeeBoarding.Click();
            Thread.Sleep(2000);
            var employeeBoardingPage = wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("//button[contains(@class, 'primary')]")));
            Thread.Sleep(2000);
            employeeBoardingPage.Click();
            Thread.Sleep(2000);
        }

        public void shift()
        {
            Thread.Sleep(2000);
            WebDriverWait wait = new WebDriverWait(Driver, timeOut);
            Thread.Sleep(2000);
            Thread.Sleep(2000);
            var sidebar = wait.Until(ExpectedConditions.ElementIsVisible(By.LinkText("Time & Attendance")));
            sidebar.Click();
            var configuration = wait.Until(ExpectedConditions.ElementIsVisible(By.LinkText("Configuration")));
            configuration.Click();
            var shift = wait.Until(ExpectedConditions.ElementIsVisible(By.LinkText("Shifts")));
            shift.Click();
            Thread.Sleep(2000);
            var shiftBtn = wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("//button[contains(@class, 'primary')]")));
            shiftBtn.Click();
            Thread.Sleep(2000);
        }
        public void clocking()
        {
            Thread.Sleep(2000);
            WebDriverWait wait = new WebDriverWait(Driver, timeOut);
            Thread.Sleep(2000);
            Thread.Sleep(2000);
            var sidebar = wait.Until(ExpectedConditions.ElementIsVisible(By.LinkText("Time & Attendance")));
            sidebar.Click();
            var operations = wait.Until(ExpectedConditions.ElementIsVisible(By.LinkText("Operations")));
            operations.Click();
            var clocking = wait.Until(ExpectedConditions.ElementIsVisible(By.LinkText("Clockings")));
            clocking.Click();
            Thread.Sleep(2000);
            Driver.FindElement(By.XPath("//button[contains(@class, 'primary')]")).Click();
            Thread.Sleep(2000);
        }
        public void shiftRollOver()
        {
            Thread.Sleep(2000);
            WebDriverWait wait = new WebDriverWait(Driver, timeOut);
            Thread.Sleep(2000);
            Thread.Sleep(2000);
            var sidebar = wait.Until(ExpectedConditions.ElementIsVisible(By.LinkText("Time & Attendance")));
            sidebar.Click();
            var operations = wait.Until(ExpectedConditions.ElementIsVisible(By.LinkText("Operations")));
            operations.Click();
            var shiftRollover = wait.Until(ExpectedConditions.ElementIsVisible(By.LinkText("Shift Rollover")));
            shiftRollover.Click();
            Thread.Sleep(2000);

        }
        public void report()
        {
            Thread.Sleep(2000);
            WebDriverWait wait = new WebDriverWait(Driver, timeOut);
            Thread.Sleep(2000);
            Thread.Sleep(2000);
            var sidebar = wait.Until(ExpectedConditions.ElementIsVisible(By.LinkText("Reports")));
            sidebar.Click();
            var operations = wait.Until(ExpectedConditions.ElementIsVisible(By.LinkText("T&A")));
            operations.Click();


        }
        public void employeeDelete()
        {
            Thread.Sleep(2000);
            WebDriverWait wait = new WebDriverWait(Driver, timeOut);
            Thread.Sleep(2000);
            Thread.Sleep(2000);
            var sidebar = wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("//span[contains(@class, 'text')]")));
            sidebar.Click();
            var employeeBoarding = wait.Until(ExpectedConditions.ElementIsVisible(By.LinkText("Employee on-Boarding")));
            employeeBoarding.Click();
            Thread.Sleep(2000);
            //var employeeBoardingPage = wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("//button[contains(@class, 'primary')]")));
            //employeeBoardingPage.Click();
            //Thread.Sleep(2000);
        }
        public void ShiftCopy()
        {
            Thread.Sleep(2000);
            WebDriverWait wait = new WebDriverWait(Driver, timeOut);
            Thread.Sleep(2000);
            Thread.Sleep(2000);
            var sidebar = wait.Until(ExpectedConditions.ElementIsVisible(By.LinkText("Time & Attendance")));
            sidebar.Click();
            var operations = wait.Until(ExpectedConditions.ElementIsVisible(By.LinkText("Operations")));
            operations.Click();
            var clocking = wait.Until(ExpectedConditions.ElementIsVisible(By.LinkText("Shift Copy")));
            clocking.Click();
            Thread.Sleep(2000);
           
        }
    }
}
