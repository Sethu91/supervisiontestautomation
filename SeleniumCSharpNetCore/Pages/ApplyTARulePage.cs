﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace SuperViosionAutomationTester.Pages
{
    public class ApplyTARulePage
    {
        TimeSpan timeOut = new TimeSpan(0, 0, 25, 0, 0);
        private IWebDriver Driver;

        public ApplyTARulePage(IWebDriver driver)
        {
            Driver = driver;
        }
        public void applyTARules(dynamic data)
        {
            WebDriverWait wait = new WebDriverWait(Driver, timeOut);

            var employees = wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("//button[contains(@class, 'btn-primary')]")));
            employees.Click();
            var selectFilter = wait.Until(ExpectedConditions.ElementIsVisible(By.CssSelector(".btn-success:nth-child(2)")));
            selectFilter.Click();
            var close = wait.Until(ExpectedConditions.ElementIsVisible(By.CssSelector(".modal-xl .modal-footer > .btn")));
            close.Click();
            Thread.Sleep(2000);


            var startDate = wait.Until(ExpectedConditions.ElementIsVisible(By.Id("StartDate")));
            startDate.SendKeys(Convert.ToString(data.startDate));
            var endDate = wait.Until(ExpectedConditions.ElementIsVisible(By.Id("EndDate")));
            endDate.SendKeys(Convert.ToString(data.endDate));

            var applyTARules = wait.Until(ExpectedConditions.ElementIsVisible(By.CssSelector(".blue > .ng-tns-c25-6")));
            applyTARules.Click();

        }
    }
}
