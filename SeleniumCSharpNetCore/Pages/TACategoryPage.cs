﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Text;

namespace SuperViosionAutomationTester.Pages
{
    public class TACategoryPage
    {
        TimeSpan timeOut = new TimeSpan(0, 0, 25, 0, 0);
        private IWebDriver Driver;

        public TACategoryPage(IWebDriver driver)
        {
            Driver = driver;
        }
        public void createTACategory(dynamic data)
        {
            WebDriverWait wait = new WebDriverWait(Driver, timeOut);
            var taCategory = wait.Until(ExpectedConditions.ElementIsVisible(By.Id("TACategory_TnACatName")));
            taCategory.SendKeys(data.Description);

            var shiftType = wait.Until(ExpectedConditions.ElementIsVisible(By.Id("TACategory_ShiftType4")));
            shiftType.SendKeys(data.ShiftType);

            var reportingMethod = wait.Until(ExpectedConditions.ElementIsVisible(By.Id("TACategory_ReportingMethod2")));
            reportingMethod.SendKeys(data.ReportingMethod);

            var save = wait.Until(ExpectedConditions.ElementIsVisible(By.CssSelector(".blue > span")));
            save.Click();

        }
    }
}
