﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;


namespace SuperViosionAutomationTester.Pages.Negative
{

    public class NegativeShiftPage 
    {
        TimeSpan timeOut = new TimeSpan(0, 0, 25, 0, 0);
        private IWebDriver Driver;

        public NegativeShiftPage(IWebDriver driver)
        {
            Driver = driver;
        }


        public void negativeShiftProcess(dynamic data) 
        {
            WebDriverWait wait = new WebDriverWait(Driver, timeOut);
            var shiftduration = wait.Until(ExpectedConditions.ElementIsVisible(By.Id("Shift_Duration")));
            shiftduration.SendKeys(Convert.ToString(data.Shiftduration));
            var validation = wait.Until(ExpectedConditions.ElementIsVisible(By.CssSelector(".form-control-feedback")));
            var assert = validation.Displayed;
            Assert.True(assert);
        }



    }

}