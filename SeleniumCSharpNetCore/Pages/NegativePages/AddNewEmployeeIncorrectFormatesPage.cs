﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Threading;

namespace SuperViosionAutomationTester.Pages.Negative
{

    public class AddNewEmployeeIncorrectFormatesPage
    {
        TimeSpan timeOut = new TimeSpan(0, 0, 25, 0, 0);
        private IWebDriver Driver;

        public AddNewEmployeeIncorrectFormatesPage(IWebDriver driver)
        {
            Driver = driver;
        }


        public void addNewEmployeeIncorrectFormatesPage(dynamic data) 
        {
            Thread.Sleep(2000);
            Thread.Sleep(2000);
            WebDriverWait wait = new WebDriverWait(Driver, timeOut);
            var txtEmployeeFirstName = wait.Until(ExpectedConditions.ElementIsVisible(By.Id("EmployeeFirstName")));
            txtEmployeeFirstName.SendKeys(Convert.ToString(data.FirstName));
            Thread.Sleep(2000);
            var txtEmployeeLastName = wait.Until(ExpectedConditions.ElementIsVisible(By.Id("Employee_LastName")));
            txtEmployeeLastName.SendKeys(Convert.ToString(data.LastName));
            Thread.Sleep(2000);
            var txtEmployeeNumber = wait.Until(ExpectedConditions.ElementIsVisible(By.Id("Employee_EmploymentNumber")));
            txtEmployeeNumber.SendKeys(Convert.ToString(data.EmployeeNumber));
            Thread.Sleep(2000);
            var txtEmployeeIdNumber = wait.Until(ExpectedConditions.ElementIsVisible(By.Id("Employee_IdNumber")));
            txtEmployeeIdNumber.SendKeys(Convert.ToString(data.IdNumber));
            Thread.Sleep(2000);
            var txtEmployeeBirthDate = wait.Until(ExpectedConditions.ElementIsVisible(By.Id("Employee_DateOfBirth")));
            txtEmployeeBirthDate.SendKeys(Convert.ToString(data.DateOfBirth));
            Thread.Sleep(2000);
            var txtEmployeePassportNumber = wait.Until(ExpectedConditions.ElementIsVisible(By.Id("Employee_PassportNumber")));
            txtEmployeePassportNumber.SendKeys(Convert.ToString(data.PassportNumber));
            Thread.Sleep(2000);
            var txtEmployeeContactNumber = wait.Until(ExpectedConditions.ElementIsVisible(By.Id("Employee_ContactNumber")));
            txtEmployeeContactNumber.SendKeys(Convert.ToString(data.ContactNumber));
            Thread.Sleep(2000);
            var txtEmployeeEmail = wait.Until(ExpectedConditions.ElementIsVisible(By.Id("Employee_Email")));
            txtEmployeeEmail.SendKeys(Convert.ToString(data.Email));
            Thread.Sleep(2000);
            var txtEmployeeDateEngagement = wait.Until(ExpectedConditions.ElementIsVisible(By.Id("Employee_DateOfEngagement")));
            txtEmployeeDateEngagement.SendKeys(Convert.ToString(data.DateOfemployment));
            Thread.Sleep(2000);
            var txtEmployeeDateResignation = wait.Until(ExpectedConditions.ElementIsVisible(By.Id("Employee_DateOfResignation")));
            txtEmployeeDateResignation.SendKeys(Convert.ToString(data.DateOfResignation));
         
            Thread.Sleep(2000);

            var validation = Driver.FindElement(By.XPath("//div[@class ='modal-footer']//child::button[2]"));
            
            var assert = validation.Enabled;
            Assert.True(assert);
        }



    }

}