﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace SuperViosionAutomationTester.Pages
{
    public class LoginPage
    {
        TimeSpan timeOut = new TimeSpan(0, 0, 25, 0, 0);
        private IWebDriver Driver;

        public LoginPage(IWebDriver driver)
        {
            Driver = driver;
        }

      
        public void LoginProcess(dynamic data)
        {
            WebDriverWait wait = new WebDriverWait(Driver, timeOut);

           
            var elemental = wait.Until(ExpectedConditions.ElementIsVisible(By.LinkText("Change")));
            elemental.Click();
            Thread.Sleep(2000);
            Driver.FindElement(By.XPath("//span[contains(@class, 'switch-')]")).Click();
            Thread.Sleep(2000);
            var tenancyName = wait.Until(ExpectedConditions.ElementIsVisible((By.Id("tenancyNameInput"))));
            tenancyName.SendKeys(data.tenancyName);
            Thread.Sleep(2000);
            Driver.FindElement(By.XPath("//button[contains(@class, 'btn-primary')]")).Click();
            Thread.Sleep(1000);
            var txtUserName = wait.Until(ExpectedConditions.ElementIsVisible(By.Name("userNameOrEmailAddress")));
            txtUserName.SendKeys(data.UserName);
            Thread.Sleep(1000);
            var txtPassword = wait.Until(ExpectedConditions.ElementIsVisible(By.Name("password")));
            txtPassword.SendKeys(data.Password);
            Driver.FindElement(By.XPath("//button[contains(@class, 'btn-primary')]")).Click();
            Thread.Sleep(2000);
        }

      
    }
}
