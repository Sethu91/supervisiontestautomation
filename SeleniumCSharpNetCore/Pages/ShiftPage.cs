﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using SuperViosionAutomationTester.TimeCapsules;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace SuperViosionAutomationTester.Pages
{
    public class ShiftPage
    {
        TimeSpan timeOut = new TimeSpan(0, 0, 25, 0, 0);
        private DriverHelper _driverHelper;
        private IWebDriver Driver;
        SuperVisionTimeCapsule timeCapsulePage;
        public ShiftPage(IWebDriver driver)
        {
            Driver = driver;
            DriverHelper _driverHelper = new DriverHelper();
            timeCapsulePage = new SuperVisionTimeCapsule(driver);
        }
        public void createShift(dynamic data)
        {
            WebDriverWait wait = new WebDriverWait(Driver, timeOut);

            var shift = wait.Until(ExpectedConditions.ElementIsVisible(By.Id("ShiftDescription")));
            shift.SendKeys(data.Description);

     

            timeCapsulePage.GetTimeClickstartTimeShift(Convert.ToString(data.Starttime));

            timeCapsulePage.GetTimeClickendTimeShift(Convert.ToString(data.Endtime));

            var shiftduration = wait.Until(ExpectedConditions.ElementIsVisible(By.Id("Shift_Duration")));
            shiftduration.SendKeys(Convert.ToString(data.Shiftduration));


            var tab1 = wait.Until(ExpectedConditions.ElementIsVisible(By.CssSelector("#static-1 b")));
            tab1.Click();

            var optionsSr = wait.Until(ExpectedConditions.ElementIsVisible(By.Id("Shift_ShiftStartClockingOption")));

            var optionsSrElement = new SelectElement(optionsSr);
            Thread.Sleep(2000);
            optionsSrElement.SelectByText(Convert.ToString(data.optionsSr));
            var logEarlyClockings = wait.Until(ExpectedConditions.ElementIsVisible(By.CssSelector(".col-md-6:nth-child(1) > .ng-star-inserted:nth-child(3) span")));
            logEarlyClockings.Click();

            var tab2 = wait.Until(ExpectedConditions.ElementIsVisible(By.CssSelector("#static-3 b")));
            tab2.Click();

            var optionsEr = wait.Until(ExpectedConditions.ElementIsVisible(By.Id("Shift_ShiftEndClockingOption")));
            optionsEr.Click();
            var optionsErElement = new SelectElement(optionsEr);
            Thread.Sleep(2000);
            optionsErElement.SelectByText(Convert.ToString(data.optionsEr));

            var clockOutWindow = wait.Until(ExpectedConditions.ElementIsVisible(By.CssSelector(".ng-star-inserted:nth-child(7) > .kt-checkbox > span")));
            clockOutWindow.Click();
            var clockOutWindowMinutes = wait.Until(ExpectedConditions.ElementIsVisible(By.Id("Shift_ClockOutWindowMinutes")));
            clockOutWindowMinutes.SendKeys(Convert.ToString(data.clockOutWindowMinutes));


            //var tab3 = wait.Until(ExpectedConditions.ElementIsVisible(By.CssSelector("#static-6 b")));
            //tab3.Click();
            //var normalTime = wait.Until(ExpectedConditions.ElementIsVisible(By.Id("NormalTime")));
            //normalTime.Click();
            //var elementNormalTime = wait.Until(ExpectedConditions.ElementIsVisible(By.CssSelector(".ng-star-inserted:nth-child(1) > td .btn")));
            //var cycleElement = new SelectElement(elementNormalTime);
            //Thread.Sleep(2000);
            //cycleElement.SelectByText("NT - Day");

            //var overTime = wait.Until(ExpectedConditions.ElementIsVisible(By.CssSelector(".col:nth-child(1) > .form-group:nth-child(3) .input-group-append > .btn")));
            //overTime.Click();
            //var elementOverTime = wait.Until(ExpectedConditions.ElementIsVisible(By.CssSelector(".ng-star-inserted:nth-child(1) > td .btn")));
            //elementOverTime.Click();
            //var leavealLocation = wait.Until(ExpectedConditions.ElementIsVisible(By.CssSelector(".col:nth-child(2) > .form-group:nth-child(1) .input-group-append > .btn")));
            //leavealLocation.Click();
            //var elementLeavealLocation = wait.Until(ExpectedConditions.ElementIsVisible(By.CssSelector(".ng-star-inserted:nth-child(1) > td .btn")));
            //elementLeavealLocation.Click();
            //var publicHoliday = wait.Until(ExpectedConditions.ElementIsVisible(By.CssSelector(".col:nth-child(2) > .form-group:nth-child(5) .input-group-append > .btn")));
            //publicHoliday.Click();
            //var elementPublicHoliday = wait.Until(ExpectedConditions.ElementIsVisible(By.CssSelector(".ng-star-inserted:nth-child(1) > td .btn")));
            //elementPublicHoliday.Click();
            //var allowPublicHolidayOvertime = wait.Until(ExpectedConditions.ElementIsVisible(By.CssSelector(".form-group:nth-child(6) .input-group-append > .btn")));
            //allowPublicHolidayOvertime.Click();
            //var Publicholidayovertime = wait.Until(ExpectedConditions.ElementIsVisible(By.CssSelector(".ng-star-inserted > .input-group > .input-group-append > .btn")));
            //Publicholidayovertime.Click();
            #region Shift usable code
            //var radioIsNightShift = wait.Until(ExpectedConditions.ElementIsVisible(By.CssSelector(".col-md-4:nth-child(1) span")));
            //radioIsNightShift.Click();
            // driver.FindElement(By.CssSelector("#static-3 b")).Click();
            //driver.FindElement(By.CssSelector("#static-1 b")).Click();
            //driver.FindElement(By.CssSelector(".modal-body > .row > .col-md-6:nth-child(1)")).Click();
            //driver.FindElement(By.CssSelector(".col-md-4:nth-child(1) span")).Click();
            //var logEarlyClockings = wait.Until(ExpectedConditions.ElementIsVisible(By.Id("Shift_IsEarlyClockStartTime")));
            //logEarlyClockings.Click();
            //var optionsSr = wait.Until(ExpectedConditions.ElementIsVisible(By.Id("Shift_ShiftStartClockingOption")));
            //driver.FindElement(By.CssSelector(".col-md-6:nth-child(1) > .ng-star-inserted:nth-child(3) span")).Click();
            //var optionsSrElement = new SelectElement(optionsSr);
            //Thread.Sleep(2000);
            //optionsSrElement.SelectByText(Convert.ToString(data.optionsSr));
            //driver.FindElement(By.CssSelector(".col-md-6:nth-child(1) > .ng-star-inserted:nth-child(3) span")).Click();
            //driver.FindElement(By.CssSelector("#static-3 b")).Click();
            //driver.FindElement(By.CssSelector(".ng-star-inserted:nth-child(7) > .kt-checkbox > span")).Click();
            //driver.FindElement(By.Id("Shift_ClockOutWindowMinutes")).Click();
            //driver.FindElement(By.Id("Shift_ClockOutWindowMinutes")).SendKeys("15");
            //driver.FindElement(By.CssSelector(".m-checkbox-list:nth-child(5)")).Click();
            //driver.FindElement(By.CssSelector("#static-6 b")).Click();
            //driver.FindElement(By.Id("ShiftDescription")).Click();
            //driver.FindElement(By.Id("ShiftDescription")).SendKeys("jdj");
            //driver.FindElement(By.CssSelector(".blue > span")).Click();
            //driver.FindElement(By.Name("filterText")).Click();
            //driver.FindElement(By.Name("filterText")).SendKeys("d");
            //driver.FindElement(By.CssSelector(".kt-form > .row .btn")).Click();
            //driver.FindElement(By.CssSelector(".kt-form > .row .btn")).Click();
            //driver.FindElement(By.LinkText("2")).Click();
            //driver.FindElement(By.LinkText("3")).Click();
            //driver.FindElement(By.CssSelector(".ng-tns-c19-4 .ui-dropdown-trigger-icon")).Click();
            //driver.FindElement(By.CssSelector(".ng-tns-c19-4:nth-child(7) .ng-star-inserted")).Click();
            //driver.FindElement(By.CssSelector(".ng-tns-c25-3:nth-child(22) .dropdown-toggle")).Click();
            //driver.FindElement(By.LinkText("Delete")).Click();
            #endregion



            var save = wait.Until(ExpectedConditions.ElementIsVisible(By.CssSelector(".blue > span")));
            save.Click();

        }
    }
}
