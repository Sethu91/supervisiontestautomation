﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using NUnit.Framework;
using OpenQA.Selenium.Support.UI;


namespace SuperViosionAutomationTester.Pages
{
    public class ShiftCopyPage 
    {
        TimeSpan timeOut = new TimeSpan(0, 0, 25, 0, 0);
        private DriverHelper _driverHelper;
        private IWebDriver Driver;
        public ShiftCopyPage(IWebDriver driver) 
        {
            Driver = driver;
        }
        public void CopyShift(dynamic data) 
        {
            WebDriverWait wait = new WebDriverWait(Driver, timeOut);

            Thread.Sleep(2000);
            Thread.Sleep(2000);
            var selectCopyFromEmp = wait.Until(ExpectedConditions.ElementIsVisible(By.CssSelector(".ng-invalid:nth-child(3)")));
            var selectCopyFromEmpElement = new SelectElement(selectCopyFromEmp);
            selectCopyFromEmpElement.SelectByText(data.EmployeeToCopyFrom);
            Thread.Sleep(2000);
            Thread.Sleep(2000);
            //Driver.FindElement(By.XPath("//button[contains(@class, 'primary')]")).Click();
            Thread.Sleep(2000);
            var selectCopyToEmp = wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("//button[contains(@class, 'primary')]")));
            selectCopyToEmp.Click();
            var close = wait.Until(ExpectedConditions.ElementIsVisible(By.CssSelector(".modal-xl .modal-footer > .btn")));
            close.Click();
            Thread.Sleep(2000);
            Thread.Sleep(2000);
            var dateFromRange = wait.Until(ExpectedConditions.ElementIsVisible(By.Id("StartRange")));
            dateFromRange.Click();
            dateFromRange.Clear();
            dateFromRange.SendKeys(Convert.ToString(data.FromDate));
            Thread.Sleep(2000);
            Thread.Sleep(2000);
            var dateToRange = wait.Until(ExpectedConditions.ElementIsVisible(By.Id("EndRange")));
            dateToRange.Click();
            dateToRange.Clear();
            dateToRange.SendKeys(Convert.ToString(data.ToDate));
            Thread.Sleep(2000);
            Thread.Sleep(2000);
            var submit = wait.Until(ExpectedConditions.ElementIsVisible(By.CssSelector(".btn > .ng-tns-c25-6")));
            submit.Click();
            Thread.Sleep(2000);
            Thread.Sleep(2000);
        }
        
    }

   
}
