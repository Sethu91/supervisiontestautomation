﻿using SuperViosionAutomationTester.Pages;
using System;
using System.Collections.Generic;
using System.Text;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;

namespace SuperViosionAutomationTester.Steps
{
    [Binding]
    public class ShiftSteps
    {
        private DriverHelper _driverHelper;
        ShiftPage shiftPage;
        public ShiftSteps(DriverHelper driverHelper)
        {
            _driverHelper = driverHelper;
            shiftPage = new ShiftPage(_driverHelper.Driver);

        }
        [Given(@"Add New Shift")]
        public void GivenAddNewShift(Table table)
        {
            dynamic data = table.CreateDynamicInstance();
            shiftPage.createShift(data);
          
        }
        [Given(@"Assert required field")]
        public void GivenAssertRequiredField()
        {
            ScenarioContext.Current.Pending();
        }



    }
}
