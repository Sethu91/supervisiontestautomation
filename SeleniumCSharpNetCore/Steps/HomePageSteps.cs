﻿using SuperViosionAutomationTester.Pages;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using TechTalk.SpecFlow;

namespace SuperViosionAutomationTester.Steps
{
    [Binding]
    public class HomePageSteps
    {
        private DriverHelper _driverHelper;
        HomePage homePage;


        public HomePageSteps(DriverHelper driverHelper)
        {
            _driverHelper = driverHelper;
            homePage = new HomePage(_driverHelper.Driver);

        }
        [Given(@"Navigates to Pay Rates")]
        public void GivenNavigatesToPayRates()
        {

            homePage.PayRates();
        }
        [Given(@"Navigates to Work Cycles")]
        public void GivenNavigatesToWorkCycles()
        {
            homePage.WorkCycles();
        }
        [Given(@"Navigates to T&A Categories")]
        public void GivenNavigatesToTACategories()
        {
            homePage.TAndACategories();
        }
        [Given(@"Navigates to Apply T&A Rules")]
        public void GivenNavigatesToApplyTARules()
        {
            homePage.ApplyTAndARules();
        }
        [Given(@"And Navigate to Employee Onboarding")]
        public void GivenAndNavigateToEmployeeOnboarding()
        {
            homePage.employeeOnBoarding();
        }

        [Given(@"And Navigate to Shift page")]
        public void GivenAndNavigateToShiftPage()
        {
            homePage.shift();
        }
        [Given(@"Navigate to Shift page")]
        public void GivenNavigateToShiftPage()
        {
            homePage.shift();
        }



        [Given(@"And Navigate to clocking page")]
        public void GivenAndNavigateToClockingPage()
        {
            homePage.clocking();
        }
        [Given(@"And Navigate to Shift Rollover page")]
        public void GivenAndNavigateToShiftRolloverPage()
        {
            homePage.shiftRollOver();
        }
        [Given(@"And Navigate to Report")]
        public void GivenAndNavigateToReport()
        {
            homePage.report();
        }
        [Given(@"And Navigate to Employee Onboarding Coypu")]
        public void GivenAndNavigateToEmployeeOnboardingCoypu()
        {
            ScenarioContext.Current.Pending();
        }
        [Given(@"And Navigate to Employee Onboarding to delete")]
        public void GivenAndNavigateToEmployeeOnboardingToDelete()
        {
            homePage.employeeDelete();
        }

        [Given(@"And Navigate to Shift copy page")]
        public void GivenAndNavigateToShiftCopyPage()
        {
            homePage.ShiftCopy();
        }

    }
}
