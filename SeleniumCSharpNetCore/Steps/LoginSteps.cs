﻿using Coypu;

using Microsoft.VisualStudio.TestTools.UnitTesting;
using NUnit.Framework;
using SuperViosionAutomationTester.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;
using UnitTestProject1.DatabaseHelpers;

namespace SuperViosionAutomationTester.Steps
{

    [Binding]
    public class LoginSteps
    {

        private DriverHelper _driverHelper;
        LoginPage loginPage;

        public LoginSteps(DriverHelper driverHelper)
        {
            _driverHelper = driverHelper;
            loginPage = new LoginPage(_driverHelper.Driver);
        }

        [Given(@"Logs in as CPE Tenant")]
        public void LogsinasCPETenant(Table table)
        {
            _driverHelper.Driver.Navigate().GoToUrl("http://10.0.130.7:4200/account/login");
            dynamic data = table.CreateDynamicInstance();
            loginPage.LoginProcess(data);
        }
        [Given(@"Logs in as CPE Tenant Coypu")]
        public void GivenLogsInAsCPETenantCoypu(Table table)
        {
            var session = new SessionConfiguration()
            {
                Browser = Coypu.Drivers.Browser.Chrome,
                AppHost = "10.0.130.7:4200",
                Timeout = TimeSpan.FromSeconds(60)
            };
            var browser = new BrowserSession(session);
            browser.Visit("/account/login");
           
            browser.ClickLink("Change");
            browser.FindCss(".kt-switch span").Click();
            browser.FindId("tenancyNameInput").FillInWith("TBD");
            
            browser.ClickButton("Switch to the tenant");
            browser.FillIn("userNameOrEmailAddress").With("Sipho");
            browser.FillIn("password").With("Fourier123");
            browser.ClickButton("Log in");

            browser.ClickLink("Employee Management");
            browser.ClickLink("Employee on-Boarding");

            browser.FindCss(".blue:nth-child(2)").Click();

            browser.FindId("EmployeeFirstName").FillInWith("seyh");
            browser.FindId("Employee_LastName").FillInWith("hhhh");
            browser.FindId("Employee_EmploymentNumber").FillInWith("77711122");
            browser.FindId("Employee_IdNumber").FillInWith("9805235856087");
            browser.ClickLink("Categories");
            browser.FindCss(".col-md-6:nth-child(1) > .form-group:nth-child(3) .input-group-append > .btn").Click();
            var btn = browser.FindAllCss(".ng-star-inserted:nth-child(2) > td .btn");
         

            var a = 1;
            //var popup = browser.FindWindow("Select T&A category");
            //var popup = browser.FindCss(".in > .modal-dialog > .modal-content > .modal-header").Name.ToString();
            //popup.FillIn("filterText").With("Sipho");
            //var popp = browser.FindWindow(popup);
            //popp.FillIn("filterText").With("Sipho");
        }

        
        [Given(@"Compare Reports")]
        public void GivenCompareReports()
        {
          
            //var helper = new DBHelper();
            //var data = helper.Shifter();
        }
      
    }
}


