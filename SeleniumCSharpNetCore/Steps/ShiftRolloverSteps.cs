﻿using SuperViosionAutomationTester.Pages;
using System;
using System.Collections.Generic;
using System.Text;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;

namespace SuperViosionAutomationTester.Steps
{
    [Binding]
    public class ShiftRolloverSteps
    {
        private DriverHelper _driverHelper;
        ShiftRollOverPage shiftRollOverPage;


        public ShiftRolloverSteps(DriverHelper driverHelper)
        {
            _driverHelper = driverHelper;
            shiftRollOverPage = new ShiftRollOverPage(_driverHelper.Driver);

        }
       
        [Given(@"Add New ShiftRollover")]
        public void GivenAddNewShiftRollover(Table table)
        {
            dynamic data = table.CreateDynamicInstance();
            shiftRollOverPage.shiftRollOver(data);
        }

    }
}
