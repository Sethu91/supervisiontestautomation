﻿using NUnit.Framework;
using SuperViosionAutomationTester.Pages;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;

namespace SuperViosionAutomationTester.Steps
{
    [Binding]
    public class AddEmployeeSteps
    {
        private DriverHelper _driverHelper;
        AddNewEmployeePage addNewEmployeePage;

        public AddEmployeeSteps(DriverHelper driverHelper)
        {
            _driverHelper = driverHelper;
            addNewEmployeePage = new AddNewEmployeePage(_driverHelper.Driver);
        }

        [Given(@"Add New Employee")]
        public void GivenAddNewEmployee(Table table)
        {
            dynamic data = table.CreateDynamicInstance();
            addNewEmployeePage.addNewEmployee(data);
        }
        [Given(@"Assert error message")]
        public void GivenAssertErrorMessage()
        {
            ScenarioContext.Current.Pending();
        }

    }
}
