﻿using SuperViosionAutomationTester.Pages;
using System;
using System.Collections.Generic;
using System.Text;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;

namespace SuperViosionAutomationTester.Steps
{
    [Binding]
   public class ApplyTARuleSteps
    {
        private DriverHelper _driverHelper;
        ApplyTARulePage applyTARulePage;
        public ApplyTARuleSteps(DriverHelper driverHelper)
        {
            _driverHelper = driverHelper;
            applyTARulePage = new ApplyTARulePage(_driverHelper.Driver);

        }
        [Given(@"Add New T&A Rule")]
        public void GivenAddNewTARule(Table table)
        {
            dynamic data = table.CreateDynamicInstance();
            applyTARulePage.applyTARules(data);

        }
        [Given(@"Assert required field")]
        public void GivenAssertRequiredField()
        {
            ScenarioContext.Current.Pending();
        }
    }
}
