﻿using NUnit.Framework;
using SuperViosionAutomationTester.Pages;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;

namespace SuperViosionAutomationTester.Steps.EditSteps
{
    [Binding]
    public class ClockingEditSteps
    {
        private DriverHelper _driverHelper;
        AddNewEmployeePage addNewEmployeePage;

        public ClockingEditSteps(DriverHelper driverHelper)
        {
            _driverHelper = driverHelper;
            addNewEmployeePage = new AddNewEmployeePage(_driverHelper.Driver);
        }

    }
}
