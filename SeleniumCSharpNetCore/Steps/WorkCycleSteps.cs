﻿using SuperViosionAutomationTester.Pages;
using System;
using System.Collections.Generic;
using System.Text;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;

namespace SuperViosionAutomationTester.Steps
{
    [Binding]
   public class WorkCycleSteps
    {
        private DriverHelper _driverHelper;
        WorkCyclePage workCyclePage;
        public WorkCycleSteps(DriverHelper driverHelper)
        {
            _driverHelper = driverHelper;
            workCyclePage = new WorkCyclePage(_driverHelper.Driver);

        }
        [Given(@"Add New WorkCycle")]
        public void GivenAddNewWorkCycle(Table table)
        {
            dynamic data = table.CreateDynamicInstance();
            workCyclePage.createWorkCycle(data);

        }
        [Given(@"Assert required field")]
        public void GivenAssertRequiredField()
        {
            ScenarioContext.Current.Pending();
        }
    }
}
