﻿using NUnit.Framework;
using SuperViosionAutomationTester.Pages;
using SuperViosionAutomationTester.Pages.DeletePage;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;

namespace SuperViosionAutomationTester.Steps.DeleteSteps
{
    [Binding]
    public class EmployeeDeleteSteps
    {
        private DriverHelper _driverHelper;
        EmployeeDeletePage employeeDeletePage;

        public EmployeeDeleteSteps(DriverHelper driverHelper)
        {
            _driverHelper = driverHelper;
            employeeDeletePage = new EmployeeDeletePage(_driverHelper.Driver);
        }

        [Given(@"Employee delete")]
        public void GivenEmployeeDelete(Table table)
        {
            dynamic data = table.CreateDynamicInstance();
            employeeDeletePage.deleteEmp(data);
        }



    }
}
