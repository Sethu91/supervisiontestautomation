﻿using System;
using System.Collections.Generic;
using System.Text;
using SuperViosionAutomationTester.Pages;

using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;

namespace SuperViosionAutomationTester.Steps
{
    [Binding]
    public class ShiftCopySteps
    {
        private DriverHelper _driverHelper;
        ShiftCopyPage shiftCopyPage;

        public ShiftCopySteps(DriverHelper driverHelper)
        {
            _driverHelper = driverHelper;
            shiftCopyPage = new ShiftCopyPage(_driverHelper.Driver);

        }

        [Given(@"Add Shift Copy")]
        public void GivenAddShiftCopy(Table table)
        {
            dynamic data = table.CreateDynamicInstance();
            shiftCopyPage.CopyShift(data);
        }

    }
}
