﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace SuperViosionAutomationTester.Steps
{
    public class ReportStep
    {
        TimeSpan timeOut = new TimeSpan(0, 0, 25, 0, 0);
        private IWebDriver Driver;

        public ReportStep(IWebDriver driver)
        {
            Driver = driver;
        }

        public void attendenceReport(dynamic data)
        {

            Thread.Sleep(2000);
            WebDriverWait wait = new WebDriverWait(Driver, timeOut);
            var attendances = wait.Until(ExpectedConditions.ElementIsVisible(By.LinkText("Attendances")));
            attendances.Click();
            Thread.Sleep(2000);
            Thread.Sleep(2000);
            Thread.Sleep(2000);
            //var sidebar = wait.Until(ExpectedConditions.ElementIsVisible(By.CssSelector(".dx-ellipsiseditor-icon > svg")));
            //sidebar.Click();
            //var operations = wait.Until(ExpectedConditions.ElementIsVisible(By.CssSelector(".dx-ellipsiseditor-icon > svg")));
            //operations.Click();
            //var attendances = wait.Until(ExpectedConditions.ElementIsVisible(By.CssSelector("#dx-1219cd2e-683c-07ee-e493-723da3bb243a .dxd-back-accented")));
            //attendances.Click();
            //Thread.Sleep(2000);
            //var txtEmployeeFirstName = wait.Until(ExpectedConditions.ElementIsVisible(By.CssSelector(".dx-calendar-contoured-date .dxd-back-accented")));
            //txtEmployeeFirstName.Click(); 
            //Thread.Sleep(2000);
            //var txtEmployeeLastName = wait.Until(ExpectedConditions.ElementIsVisible(By.CssSelector(".dx-scrollable-scrollbars-alwaysvisible > .dx-scrollable-wrapper > .dx-scrollable-container > .dx-scrollable-content > .dx-scrollview-content")));
            //txtEmployeeLastName.Click();
            //Thread.Sleep(2000);
            //var txtEmployeeNumber = wait.Until(ExpectedConditions.ElementIsVisible(By.CssSelector(".dxrd-preview-parameters-wrapper")));
            //txtEmployeeNumber.Click();
            Thread.Sleep(2000);
            var txtEmployeeIdNumber = wait.Until(ExpectedConditions.ElementIsVisible(By.CssSelector(".dxrdp-parameters-submit > .dx-button-content")));
            txtEmployeeIdNumber.Click();
            Thread.Sleep(2000);
            var validation = wait.Until(ExpectedConditions.ElementIsVisible(By.CssSelector(".dxrd-report-preview-content")));
            var assert = validation.Displayed;
            Assert.True(assert);

        }
        public void oddClockingsReport(dynamic data)
        {
            Thread.Sleep(2000);
            WebDriverWait wait = new WebDriverWait(Driver, timeOut);
            var attendances = wait.Until(ExpectedConditions.ElementIsVisible(By.LinkText("Odd Clockings")));
            attendances.Click();
            Thread.Sleep(2000);
            var txtEmployeeIdNumber = wait.Until(ExpectedConditions.ElementIsVisible(By.CssSelector(".dxrdp-parameters-submit > .dx-button-content")));
            txtEmployeeIdNumber.Click();
            Thread.Sleep(2000);
            var validation = wait.Until(ExpectedConditions.ElementIsVisible(By.CssSelector(".dxrd-report-preview-content")));
            var assert = validation.Displayed;
            Assert.True(assert);

        }
        public void lateArrivalReport(dynamic data)
        {
            Thread.Sleep(2000);
            WebDriverWait wait = new WebDriverWait(Driver, timeOut);
            var attendances = wait.Until(ExpectedConditions.ElementIsVisible(By.LinkText("Absentees")));
            attendances.Click();
            Thread.Sleep(2000);
            var txtEmployeeIdNumber = wait.Until(ExpectedConditions.ElementIsVisible(By.CssSelector(".dxrdp-parameters-submit > .dx-button-content")));
            txtEmployeeIdNumber.Click();
            Thread.Sleep(2000);
            var validation = wait.Until(ExpectedConditions.ElementIsVisible(By.CssSelector(".dxrd-report-preview-content")));
            var assert = validation.Displayed;
            Assert.True(assert);

        }
        public void earlyDepartureReport(dynamic data)
        {
            Thread.Sleep(2000);
            WebDriverWait wait = new WebDriverWait(Driver, timeOut);
            var attendances = wait.Until(ExpectedConditions.ElementIsVisible(By.LinkText("Late Arrivals")));
            attendances.Click();
            Thread.Sleep(2000);
            var txtEmployeeIdNumber = wait.Until(ExpectedConditions.ElementIsVisible(By.CssSelector(".dxrdp-parameters-submit > .dx-button-content")));
            txtEmployeeIdNumber.Click();
            Thread.Sleep(2000);
            var validation = wait.Until(ExpectedConditions.ElementIsVisible(By.CssSelector(".dxrd-report-preview-content")));
            var assert = validation.Displayed;
            Assert.True(assert);

        }
        public void exceptionManagementReport(dynamic data)
        {
            Thread.Sleep(2000);
            WebDriverWait wait = new WebDriverWait(Driver, timeOut);
            var attendances = wait.Until(ExpectedConditions.ElementIsVisible(By.LinkText("Exception Mngt")));
            attendances.Click();
            Thread.Sleep(2000);
            var txtEmployeeIdNumber = wait.Until(ExpectedConditions.ElementIsVisible(By.CssSelector(".dxrdp-parameters-submit > .dx-button-content")));
            txtEmployeeIdNumber.Click();
            Thread.Sleep(2000);
            var validation = wait.Until(ExpectedConditions.ElementIsVisible(By.CssSelector(".dxrd-report-preview-content")));
            var assert = validation.Displayed;
            Assert.True(assert);

        }
    }
}
