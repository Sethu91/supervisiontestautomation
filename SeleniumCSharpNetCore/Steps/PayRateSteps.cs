﻿using SuperViosionAutomationTester.Pages;
using System;
using System.Collections.Generic;
using System.Text;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;

namespace SuperViosionAutomationTester.Steps
{
    [Binding]
    public class PayRateSteps
    {
        private DriverHelper _driverHelper;
        PayRatePage payRatePage;
        public PayRateSteps(DriverHelper driverHelper)
        {
            _driverHelper = driverHelper;
            payRatePage = new PayRatePage(_driverHelper.Driver);

        }
        [Given(@"Add New PayRate")]
        public void GivenAddNewPayRate(Table table)
        {
            dynamic data = table.CreateDynamicInstance();
            payRatePage.createPayRate(data);

        }
        [Given(@"Assert required field")]
        public void GivenAssertRequiredField()
        {
            ScenarioContext.Current.Pending();
        }



    }
}
