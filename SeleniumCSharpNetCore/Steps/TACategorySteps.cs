﻿using SuperViosionAutomationTester.Pages;
using System;
using System.Collections.Generic;
using System.Text;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;

namespace SuperViosionAutomationTester.Steps
{
    [Binding]
    public class TACategorySteps
    {
        private DriverHelper _driverHelper;
        TACategoryPage taCategoryPage;
        public TACategorySteps(DriverHelper driverHelper)
        {
            _driverHelper = driverHelper;
            taCategoryPage = new TACategoryPage(_driverHelper.Driver);

        }
        [Given(@"Add New T&A Category")]
        public void GivenAddNewTACategory(Table table)
        {
            dynamic data = table.CreateDynamicInstance();
            taCategoryPage.createTACategory(data);

        }
        [Given(@"Assert required field")]
        public void GivenAssertRequiredField()
        {
            ScenarioContext.Current.Pending();
        }
    }
}
