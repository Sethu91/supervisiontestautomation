﻿using SuperViosionAutomationTester.Pages.NegativePages;
using System;
using System.Collections.Generic;
using System.Text;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;

namespace SuperViosionAutomationTester.Steps.NegativeSteps
{
    [Binding]
    public class AddEmployeeNeg5Steps
    {
        private DriverHelper _driverHelper;
        AddEmployeeNeg5Pages addEmployeeNeg5Pages;
        public AddEmployeeNeg5Steps(DriverHelper driverHelper)
        {
            _driverHelper = driverHelper;
            addEmployeeNeg5Pages = new AddEmployeeNeg5Pages(_driverHelper.Driver);

        }
        [Given(@"Add New Employee Duplicate Employee Number")]
        public void GivenAddNewEmployeeDuplicateEmployeeNumber(Table table)
        {
            dynamic data = table.CreateDynamicInstance();
            addEmployeeNeg5Pages.addEmployeeneg(data);
        }

    }
}
