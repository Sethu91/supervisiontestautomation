﻿
using SuperViosionAutomationTester.Pages.Negative;
using SuperViosionAutomationTester.Pages.NegativePages;
using System;
using System.Collections.Generic;
using System.Text;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;

namespace SuperViosionAutomationTester.Steps.NegativeSteps
{
    [Binding]
    public class AddEmployeeNeg1Steps
    {
        private DriverHelper _driverHelper;
        AddEmployeeNeg1Pages addEmployeeNeg1Pages;
        public AddEmployeeNeg1Steps(DriverHelper driverHelper)
        {
            _driverHelper = driverHelper;
            addEmployeeNeg1Pages = new AddEmployeeNeg1Pages(_driverHelper.Driver);

        }
        [Given(@"Add New Employee Negative")]
        public void GivenAddNewEmployeeNegative(Table table)
        {
            dynamic data = table.CreateDynamicInstance();
            addEmployeeNeg1Pages.addEployeeneg(data);
        }

    }
}
