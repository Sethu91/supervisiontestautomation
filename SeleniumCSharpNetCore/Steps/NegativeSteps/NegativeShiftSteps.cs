﻿
using SuperViosionAutomationTester.Pages.Negative;
using System;
using System.Collections.Generic;
using System.Text;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;

namespace SuperViosionAutomationTester.Steps.NegativeSteps
{
    [Binding]
    public class NegativeShiftSteps
    {
        private DriverHelper _driverHelper;
        NegativeShiftPage negativeShiftPage;
        public NegativeShiftSteps(DriverHelper driverHelper)
        {
            _driverHelper = driverHelper;
            negativeShiftPage = new NegativeShiftPage(_driverHelper.Driver);

        }
        [Given(@"Add New Shift negative")]
        public void GivenAddNewShiftNegative(Table table)
        {
            dynamic data = table.CreateDynamicInstance();
            negativeShiftPage.negativeShiftProcess(data);
        }

     
    }
}
