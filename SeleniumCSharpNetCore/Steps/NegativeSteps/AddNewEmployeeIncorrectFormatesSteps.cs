﻿using SuperViosionAutomationTester.Pages.Negative;
using SuperViosionAutomationTester.Pages.NegativePages;
using System;
using System.Collections.Generic;
using System.Text;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;

namespace SuperViosionAutomationTester.Steps.NegativeSteps
{
    [Binding]
    public class AddNewEmployeeIncorrectFormatesSteps
    {
        private DriverHelper _driverHelper;
        AddNewEmployeeIncorrectFormatesPage addNewEmployeeIncorrectFormatesPage;
        public AddNewEmployeeIncorrectFormatesSteps(DriverHelper driverHelper)
        {
            _driverHelper = driverHelper;
            addNewEmployeeIncorrectFormatesPage = new AddNewEmployeeIncorrectFormatesPage(_driverHelper.Driver);

        }
 
    
        [Given(@"Add New Employee with incorrect formates")]
        public void GivenAddNewEmployeeWithIncorrectFormates(Table table)
        {
            dynamic data = table.CreateDynamicInstance();
            addNewEmployeeIncorrectFormatesPage.addNewEmployeeIncorrectFormatesPage(data);
        }

    }
}
