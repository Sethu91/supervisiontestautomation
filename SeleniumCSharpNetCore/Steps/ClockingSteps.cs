﻿using System;
using System.Collections.Generic;
using System.Text;
using SuperViosionAutomationTester.Pages;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;

namespace SuperViosionAutomationTester.Steps
{
    [Binding]
    public class ClockingSteps
    {
        private DriverHelper _driverHelper;
        ClockingPage clockingPage;


        public ClockingSteps(DriverHelper driverHelper)
        {
            _driverHelper = driverHelper;
            clockingPage = new ClockingPage(_driverHelper.Driver);

        }
        [Given(@"Add New Clocking")]
        public void GivenAddNewClocking(Table table)
        {
            dynamic data = table.CreateDynamicInstance();
            clockingPage.createClocking(data);
        }
   
   

    }
}
